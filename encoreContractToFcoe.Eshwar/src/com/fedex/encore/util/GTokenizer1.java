/**


*/
package com.fedex.encore.util;

import java.util.Vector;
public class GTokenizer1 {

   public static Vector tokenize1(String in, String delim) {
   // Vector vout = new Vector();   
   //warning: [unchecked] unchecked call to addElement(E) as a member of the raw type java.util.Vector

      Vector<String> vout = new Vector<String>();   
      String tok = "";
      int a = 0, b = 0;

   // safeguards
      if(in == null || in.equals("")) {return vout;} 
      if(delim == null || delim.equals("")) {return vout;}

      while(in.indexOf(delim, a) != -1) {
         b = in.indexOf(delim, a);
         tok = in.substring(a,b);          	
         vout.addElement(tok);

         a = b + delim.length();
      }

      String lasttok = in.substring(a, in.length());
      vout.addElement(lasttok);
      return vout;
   }

} 
