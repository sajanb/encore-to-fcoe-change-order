package com.fedex.encore.util;

import java.util.ResourceBundle;

public class PropertyUtility {
	
	public static ResourceBundle configProperties;

	static {
		try {
			//  This will load encore.properties from the classpath
			configProperties = ResourceBundle.getBundle("encore");
		}
		catch(Exception e){
			//  What should we do here?
			System.out.println("Could not load properties: " + e);
		}
	}
	
	public static String getProperty(String prop) { 
		
		return configProperties.getString(prop);
		
	}
	
	public static String getEncryptedProperty(String prop) throws Exception { 
		
		AesEncr aes = new AesEncr("g10.k");
		
		return aes.deCrypt( configProperties.getString(prop) );
		
	}

}
