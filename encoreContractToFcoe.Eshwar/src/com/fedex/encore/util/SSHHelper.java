package com.fedex.encore.util;

import com.sshtools.j2ssh.transport.publickey.SshPublicKey;
import com.sshtools.j2ssh.transport.HostKeyVerification;
import java.lang.StringBuffer;
import java.lang.Integer;
import java.util.Properties;
import com.sshtools.j2ssh.transport.TransportProtocolException;
import com.sshtools.j2ssh.authentication.PasswordAuthenticationClient;
import com.sshtools.j2ssh.authentication.PublicKeyAuthenticationClient;
import com.sshtools.j2ssh.authentication.AuthenticationProtocolState;
import com.sshtools.j2ssh.transport.publickey.SshPrivateKeyFile;
import com.sshtools.j2ssh.transport.publickey.SshPrivateKey;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.sshtools.j2ssh.SshClient;
import com.sshtools.j2ssh.ScpClient;
import com.sshtools.j2ssh.SftpClient;
import com.sshtools.j2ssh.session.SessionChannelClient;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.Hashtable;
import java.io.InputStream;
import java.io.File;

//import com.fedex.hrt.commonclasses.HrConfigHandler;

//************************************************************************
/**
 *
 * SSHHelper This class assists in connecting to a SSH server fo SCP,
 *           SFTP or SSH command executions.
 *  @author   John McKinnon
 *  @version  1.2
 *  date     8/10/2005
 */
//************************************************************************
public class SSHHelper implements HostKeyVerification
{
  private String knownHostName = "";
  private String knownIPAddress = "";
  private String knownKeyFingerprint = "";
  private String foundHost = "";
  private String foundIPAddress = "";
  private String foundHostName = "";
  private String commandResult = null;
  private String commandError = null;
  private SshPublicKey foundKey = null;
  private boolean verbose = false;
  private SshClient ssh = null;
  private static SSHHelper helper = null;
  private SftpClient sftp = null;
  private ScpClient scp = null;
  private SessionChannelClient session;


  private static final String SYNTAX =
          "Syntax is: SSHHelper <hostName> \n" +
          "       or: SSHHelper <projectName> <action> \n" +
          "       or: SSHHelper <projectName> <action> <SourceFile> <DestinationFile>\n" +
          "       or: SSHHelper <projectName> <action> <SourceFile> <DestinationFile> <RetryCount>\n";

  //**********************************************************************
  /**
   *  The default constructor. Will allow a user needs to retrieve a
   *  hostname & key for future validation.
   */
  //**********************************************************************
  public SSHHelper()
  {
    // Shut off the default logger
    this.setDefaultLogging(false);
    // Connect to SSH server with an SSH client
    this.ssh = new SshClient();
    // Shut off verbose output
    this.setVerbose(false);
  }

  //**********************************************************************
  /**
   *  This main method allows a user to retrieve the hostname and key
   *  needed for validation (typically used in a config file).
   *    OR
   *  perform a file transfer from the command line
   */
  //**********************************************************************
  public static void main (String[] args)
  {
    // Check usage
    if (args.length == 0)
    {
      System.out.println(SYNTAX);
      System.exit(1);
    }
    else if (args.length == 1)
    {
        dumpServerInfo(args[0]);
    }
    else
    {
        performTransfer(args);
    }


  }

  //**********************************************************************
  /**
   *  This method was added to proved a command line interface that
   *  was (more or less) compatible with the HrFTP class
   *  Please note:
   *     - SFTP does not distinguish between binary/text transfers
   *     - could not determine if package supports the "append" option
   */
  //**********************************************************************
  private static void performTransfer(String[] args)
  {

      String projectName = null;
      String action = null;
      String srcFile = null;
      String desFile = null;
      String sftpServer = null;
      String sftpUser   = null;
      String sftpPswd   = null;    
      String sftpPort   = null;    
      boolean verboseMode = false;
      
      String sRetry = "1";
      int iRetry = 1;
      int iRc = 99;


      Properties props = new Properties();
      if ( ( args.length > 1 ) )
      {
          projectName = args[0];
          System.out.println( "projectName: " + projectName);
          //initalize property file and log file

          try
          {
           //HrConfigHandler cfHandler = new HrConfigHandler();
           //props = cfHandler.getProperties(projectName);
          }
          catch ( Exception e )
          {
              e.printStackTrace();
              System.err.println("ERROR: xception with config file ");
              System.exit(iRc);
          }
      }



      // first get stuff from properties file, then overlay with command
      // line arguments as applicable

      srcFile = props.getProperty("SourceFile");
      desFile = props.getProperty("DestinationFile");

      sftpServer = props.getProperty("FtpServer");
      sftpUser   = props.getProperty("FtpUser");
      sftpPort   = props.getProperty("SftpPort");
      sftpPswd   = props.getProperty("FtpPassword");
      sRetry     = props.getProperty("FtpRetryCount");




      //Set input arguments
      if ( ( args.length == 2 ) ||
           ( args.length == 4 ) ||
           ( args.length == 5 ) )
      {
         action = args[1];
         System.out.println("ARG 0: " + projectName);
         System.out.println("ARG 1: " + action);
         if ( args.length > 2 )
         {
            srcFile = args[2];
            desFile = args[3];
            System.out.println("ARG 2: " + srcFile);
            System.out.println("ARG 3: " + desFile);
         }
         if ( args.length > 4 )
         {
            sRetry = args[4];
            System.out.println("ARG 4: " + sRetry);
         }
      }
      else
      {
          System.out.println(SYNTAX);
          System.out.println( "where <project name> is: project configuration file used by config file handler\n");
          System.out.println( "      <action> is: get or put\n");
          System.out.println( "      <BinaryTransfer> : true for binary - false for ASCII transfer (default to false)\n");
          System.out.println( "      <Append> : true to append - false to overwrite (default to false)\n");
          System.out.println( "      <SourceFile> : set alternate source file and location\n");
          System.out.println( "      <DestinationFile> : set alternate destination file and location\n");
          System.exit( 1 );
      }

      helper = new SSHHelper();

      try
      {
         iRetry = Integer.parseInt(sRetry);
      }
      catch (NumberFormatException nfe)
      {
         iRetry = 2;
         System.out.println("Invalid retry argument " );
         System.out.println("Defaulting retry count to "+iRetry );
      }


      try
      {
          System.out.println("Connecting to "+sftpServer);

          helper = new SSHHelper();
          helper.setVerbose(verboseMode);
          helper.setKnownHostName( sftpServer );
          if ((sftpPort == null) || (sftpPort.length() == 0))
          {
              System.out.println("Opening with default port  ");
              helper.connectToServer();
          }
          else
          {
              int iPort = 22;
              try
              {
                 iPort = Integer.parseInt((sftpPort));
              }
              catch (Exception e)
              {
                 System.err.println("Error parsing port. Port=["+sftpPort+"]");
              }
              System.out.println("Opening with port "+iPort);
              helper.connectToServer(iPort);
          }
          SftpClient sftp = helper.getSFTPClient(sftpUser, sftpPswd);
          //sftp.cd( uploadDir );
          //sftp.lcd( srcPath );

          for (int i=0; i < iRetry; i++)
          {
              try
              {
                  if (action.equals("put"))
                  {
                     sftp.put(srcFile, desFile);
                     iRc = 0;
                     i = iRetry+1;   // force loop exit
                  }
                  else if (action.equals("get"))
                  {
                     sftp.get(srcFile, desFile);
                     iRc = 0;
                     i = iRetry+1;   // force loop exit
                  }
                  else
                  {
                      System.err.println("Invalid action "+action);
                      iRc = 5;
                      i = iRetry +1;  // force loop exit
                  }
              }
              catch  (Exception e)
              {
                  e.printStackTrace();
                  if (i == iRetry - 1)
                  {
                      iRc = 3;
                  }
                  else
                  {
                     System.out.println("Failed.. re-retrying  ");
                  }
                       
              }
          }
      }
      catch ( Exception e )
      {
          System.err.println("Error in section: FTP to "+sftpServer);
          e.printStackTrace();
          System.exit(1);
      }
      finally
      {
    	  try
    	  {
    		  helper.disconnect();
    	  }
    	  catch (IOException ioe)
    	  {
    		  // keep on trucking
    	  }
          
      }
      if (iRc == 0)
      {
          System.out.println(action+" successful to "+sftpServer+":"+desFile);
      }
      System.exit(iRc);

}

  //**********************************************************************
  /**
   *  onUnknownHost
   *  @param String a
   *  @param SshPublicKey b
   */
  //**********************************************************************
  public void onUnknownHost(String a, SshPublicKey b)
  {
    // Place-holder for possible future logic to provide an alternate
    // verification method when an unknown host is encountered
  }

  //**********************************************************************
  /**
   *  onHostKeyMismatch
   *  @param String a
   *  @param SshPublicKey b
   *  @param SshPublicKey c
   */
  //**********************************************************************
  public void onHostKeyMismatch(String a, SshPublicKey b, SshPublicKey c)
  {
    // Place-holder for possible future logic to provide an alternate
    // verification method when an unknown key is encountered.
  }

  //**********************************************************************
  /**
   *  connectToServer Establish the SSH connection 'tunnel'
   */
  //**********************************************************************
  public void connectToServer()
  throws IOException
  {
    this.ssh.connect(this.knownHostName,this);
  }

  //**********************************************************************
  /**
   *  connectToServer Establish the SSH connection 'tunnel' using a specific
   *  port other than default ssh port 22
   */
  //**********************************************************************
  public void connectToServer(int port)
  throws IOException
  {
    // using a port that was set by the instantiator
    this.ssh.connect(this.knownHostName, port, this);
    return;
  }

  //**********************************************************************
  /**
   *  verifyHost This overrides the default behavior of the interface.
   *             This method only separates the hostname and IP from the
   *             combined string returned by the server.
   *             Further validation uses the validateServer() method.
   *             This method is invoked by the interface itself when
   *             ssh.connect() is called.
   *
   *             Though the name is misleading and the return value isn't
   *             used, this method is required by the interface.
   *
   *  @param String host
   *  @param SshPublicKey pk
   *  @return boolean
   *  @throws TransportProtocolException
   */
  //**********************************************************************
  public boolean verifyHost(String host, SshPublicKey pk)
  throws TransportProtocolException
  {
    this.foundHost = host;
    this.foundKey = pk;

    StringTokenizer tokenizer = new StringTokenizer(this.foundHost, ",");
    this.foundHostName = tokenizer.nextToken();
    this.foundIPAddress = tokenizer.nextToken();

    return true;
  }

  //**********************************************************************
  /**
   *  validateServer Used to validate a SFTP server against a known
   *                 hostname & public key fingerprint. A validation
                     failure will cause an SSHValidationException to be
   *                 thrown.
   *
   *  @return boolean
   *  @throws SSHValidationException
   */
  //**********************************************************************
  public boolean validateServer() throws Exception {
    if (this.verbose) {
      System.out.println("Validating server name & key fingerprint");
      System.out.println("===============================================");
      System.out.println("Found host=[" + this.foundHostName + "]");
      System.out.println("Known host=[" + this.knownHostName + "]");
      System.out.println("               -----------------               ");
      System.out.println("Found key=[" + this.foundKey.getFingerprint() + "]");
      System.out.println("Known key=[" + this.knownKeyFingerprint + "]");
    }

    if ((this.knownHostName.equals(this.foundHostName)) &&
       (this.knownKeyFingerprint.equals(this.foundKey.getFingerprint())) )
    {
      if (this.verbose)
      {
        System.out.println("===============================================");
      }
      return true;
    }
    else
    {
      if (this.verbose)
      {
        System.out.println("       HOSTNAME/KEY DOES NOT MATCH!!");
        System.out.println("===============================================");
      }

      StringBuffer exceptionMessage = new StringBuffer();
      exceptionMessage.append("Invalid host/key found.\n\n");
      exceptionMessage.append("      Found host=["
        + this.foundHost
        + "]\n");
      exceptionMessage.append(" Found host name=["
        + this.foundHostName
        + "]\n");
      exceptionMessage.append("Known valid host=["
        + this.knownHostName
        + "]\n");
      exceptionMessage.append("       Found key=["
        + this.foundKey.getFingerprint()
        + "]\n");
      exceptionMessage.append("Known valid key=["
        + this.knownKeyFingerprint
        + "]\n");

      throw new Exception(exceptionMessage.toString());
    }
  }



  //**********************************************************************
  /**
   *  dumpServerInfo Displays the server-supplied hostname and public key
   *                 fingerprint for a given host.
   */
  //**********************************************************************
  private static void dumpServerInfo(String serverName)
  {
      helper = new SSHHelper();

      helper.setVerbose(false); // Turn off debugging
      helper.setKnownHostName(serverName);
      try
      {
         helper.connectToServer();
      }
      catch (IOException ioe)
      {
         System.out.println("SSH connection failure:");
         ioe.printStackTrace();
        System.exit(1);
      }
      helper.dumpServerInfo(); // Display server hostname and fingerprint
      try
      {
         helper.disconnect();
      }
      catch (Exception exc)
      {
         // Ignore this condition
      }
  }


  //**********************************************************************
  /**
   *  dumpServerInfo Displays the server-supplied hostname and public key
   *                 fingerprint for a given host.
   */
  //**********************************************************************
  public void dumpServerInfo()
  {
    System.out.println("Retrieving hostname and key fingerprint.");
    System.out.println("===============================================");
    System.out.println("     host=[" + this.foundHostName + "]");
    System.out.println("     key=[" + this.foundKey.getFingerprint() + "]");
    System.out.println("===============================================");
    System.out.println("** NOTE: actual values are within brackets.");
  }

  //**********************************************************************
  /**
   *  getState Displays the state of the connection
   *  @return Hashtable
   */
  //**********************************************************************
  public Hashtable getState()
  {
    Hashtable state = new Hashtable();

    if (this.foundHostName != null)
    {
      state.put("SSH Server hostname", this.foundHostName);
    }
    else
    {
      state.put("SSH Server hostname", "null");
    }
    if ( (this.foundKey.getFingerprint() != null) && (this.foundKey.getFingerprint() != null) )
    {
      state.put("SSH Server fingerprint", this.foundKey.getFingerprint());
    }
    else
    {
      state.put("SSH Server fingerprint", "null");
    }
    if (this.sftp != null)
    {
      if (this.sftp.isClosed())
      {
        state.put("SFTP client state", "open");
      }
      else
      {
        state.put("SFTP client state", "closed");
      }
    }
    else
    {
      state.put("SFTP client state", "null");
    }
    if (this.scp != null)
    {
      state.put("SCP client state", "created");
    }
    else
    {
      state.put("SCP client state", "null");
    }
    if (this.ssh != null)
    {
      state.put("SSH Client authentication state", String.valueOf(this.ssh.isAuthenticated()));
      state.put("SSH Client connection state", String.valueOf(this.ssh.isConnected()));

    }
    else
    {
      state.put("SSH Client state", "null");
    }

    return state;
  }

  //**********************************************************************
  /**
   *  getSFTPClient This is used to establish and verify a SFTP connection
   *  @param String userName
   *  @param String passwd
   *  @return SftpClient
   *  @throws SSHAuthenticationException
   *  @throws IOException
   */
  //**********************************************************************
  public SftpClient getSFTPClient(String userName, String userPassword) throws Exception {
    // Init connection with username & password
    this.authenticateConnection(userName, userPassword);

    // Open an SFTP sub-client
    this.sftp = this.ssh.openSftpClient();
    return sftp;
  }

  //**********************************************************************
  /**
   *  getSFTPClient This is used to establish and verify a SFTP connection
                    once the SSH connection has already been created.
   *  @return SftpClient
   *  @throws SSHAuthenticationException
   *  @throws IOException
   */
  //**********************************************************************
  public SftpClient getSFTPClient() throws Exception {
    // Open an SFTP sub-client
    this.sftp = this.ssh.openSftpClient();
    return sftp;
  }

  //**********************************************************************
  /**
   *  getSCPClient This is used to establish and verify a SCP connection
   *  @param String userName
   *  @param String passwd
   *  @return ScpClient
   *  @throws SSHAuthenticationException
   *  @throws IOException
   */
  //**********************************************************************
  public ScpClient getSCPClient(String userName, String userPassword) throws Exception {
    // Init connection with username & password
    this.authenticateConnection(userName, userPassword);

    // Open an SCP sub-client
    this.scp = this.ssh.openScpClient();
    return scp;
  }

  //**********************************************************************
  /**
   *  getSCPClient This is used to establish and verify a SCP connection
   *               once the SSH connection has already been created.
   *  @return ScpClient
   *  @throws SSHAuthenticationException
   *  @throws IOException
   */
  //**********************************************************************
  public ScpClient getSCPClient() throws Exception {
    // Open an SCP sub-client
    this.scp = this.ssh.openScpClient();
    return scp;
  }

  //**********************************************************************
  /**
   *  openForRemoteCommands This is used to establish and verify a
   *                        SSH connection for command execution
   *  @param String userName
   *  @param String passwd
   *  @throws SSHAuthenticationException
   *  @throws IOException
   */
  //**********************************************************************
  public void openForRemoteCommands(String userName, String userPassword) throws Exception {
    // Init connection with username & password
    this.authenticateConnection(userName, userPassword);
  }

  //**********************************************************************
  /**
   *  executeCommand This is used to execute a command via SSH
   *  @param String command
   *  @return boolean result
   *  @throws IOException
   */
  //**********************************************************************
  public boolean executeCommand(String command) throws Exception {
    String stdout = null;
    String stderr = null;
    boolean result = false;
    this.resetCommandResults();

    this.session = ssh.openSessionChannel();
    this.session.executeCommand(command);
    this.commandResult = returnCommandResult();
    this.commandError = returnCommandError();

    if (this.commandError.length() > 0)
    {
      result = false;
    }
    else
    {
      result = true;
    }

    return result;
  }

  //**********************************************************************
  /**
   *  returnCommandError This is used to get the stderr from an executed
   *                     command
   *  @return String
   *  @throws IOException
   */
  //**********************************************************************
  private String returnCommandError() throws IOException
  {
    // Read the stderr from the session InputStream
    StringBuffer strbuf = new StringBuffer();
    InputStream stderr = this.session.getStderrInputStream();

    byte buffer[] = new byte[2048];
    int read;
    while((read = stderr.read(buffer)) > 0)
    {
      strbuf.append(new String(buffer, 0, read));
    }

    return strbuf.toString();
  }

  //**********************************************************************
  /**
   *  returnCommandResult This is used to get the stdout from an executed
   *                      command
   *  @return String
   *  @throws IOException
   */
  //**********************************************************************
  private String returnCommandResult() throws IOException
  {
    // Read the stdout from the session InputStream
    StringBuffer strbuf = new StringBuffer();
    InputStream stdout = this.session.getInputStream();

    byte buffer[] = new byte[8192];
    int read;
    while((read = stdout.read(buffer)) > 0)
    {
      strbuf.append(new String(buffer, 0, read));
    }

    return strbuf.toString();
  }

  //**********************************************************************
  /**
   *  resetCommandResults This is used to reset the stored commandResult
   *                      and commandError attributes
   */
  //**********************************************************************
  private void resetCommandResults()
  {
    this.commandError = "";
    this.commandResult = "";
  }

  //**********************************************************************
  /**
   *  authenticateConnectionWithPubKey Initialize the SSH connection using
   *      a public key
   *  @param String userName
   *  @param String keyFile
   *  @param String passPhrase
   */
  //**********************************************************************
  public void authenticateConnectionWithPubKey(String userName, String keyFile, String passPhrase) throws Exception {
    PublicKeyAuthenticationClient pk = new PublicKeyAuthenticationClient();
    pk.setUsername(userName);
    SshPrivateKeyFile file = SshPrivateKeyFile.parse(new File(keyFile));
    SshPrivateKey key = file.toPrivateKey(passPhrase);
    pk.setKey(key);

    if (this.verbose) {
      System.out.println("userName=[" + userName + "]");
      System.out.println("keyFile=[" + keyFile + "]");
      System.out.println("passPhrase=[" + passPhrase
        + "] **optional, null is allowed**");
    }

    // Check client authentication
    int result = this.ssh.authenticate(pk);

    if(result != AuthenticationProtocolState.COMPLETE)
    {
      // Handle authentication problems

      //        *****Authentication codes*****
      // COMPLETE - The authentication succeeded.
      //  PARTIAL - The authentication succeeded but a further
      //             authentication method is required.
      //   FAILED - The authentication failed.
      //CANCELLED - The user cancelled authentication (can only be
      //             returned when the user is prompted for information.

      StringBuffer exceptionMessage = new StringBuffer();

      exceptionMessage.append("SSH authentication error: ");

      if (result == AuthenticationProtocolState.PARTIAL)
      {
        // Close the connection
        try
        {
          this.ssh.disconnect();
        }
        catch (Exception exc)
        {
          // Ignore this condition
        }

        // append message
        exceptionMessage.append("SSH authentication incomplete.");
      }
      else if (result == AuthenticationProtocolState.FAILED)
      {
        // Close the connection
        try
        {
          this.ssh.disconnect();
        }
        catch (Exception exc)
        {
          // Ignore this condition
        }

        // append message
        exceptionMessage.append("SSH authentication failed.");
      }
      throw new Exception(exceptionMessage.toString());
    }
  }

  //**********************************************************************
  /**
   *  authenticateConnection Initialize the SSH connection
   *  @param String userName
   *  @param String userPassword
   */
  //**********************************************************************
  private void authenticateConnection(String userName, String userPassword) throws Exception {
    // Init connection with username & password
    PasswordAuthenticationClient pwd = new PasswordAuthenticationClient();
    pwd.setUsername(userName);
    pwd.setPassword(userPassword);

    if (this.verbose)
    {
      System.out.println("userName=[" + userName + "]");
      System.out.println("userPassword=[" + userPassword + "]");
    }

    // Check client authentication
    int result = this.ssh.authenticate(pwd);

    if(result != AuthenticationProtocolState.COMPLETE)
    {
      // Handle authentication problems

      //        *****Authentication codes*****
      // COMPLETE - The authentication succeeded.
      //  PARTIAL - The authentication succeeded but a further
      //             authentication method is required.
      //   FAILED - The authentication failed.
      //CANCELLED - The user cancelled authentication (can only be
      //             returned when the user is prompted for information.

      StringBuffer exceptionMessage = new StringBuffer();

      exceptionMessage.append("SSH authentication error: ");

      if (result == AuthenticationProtocolState.PARTIAL)
      {
        // Close the connection
        try
        {
          this.ssh.disconnect();
        }
        catch (Exception exc)
        {
          // Ignore this condition
        }

        // append message
        exceptionMessage.append("SSH authentication incomplete.");
      }
      else if (result == AuthenticationProtocolState.FAILED)
      {
        // Close the connection
        try
        {
          this.ssh.disconnect();
        }
        catch (Exception exc)
        {
          // Ignore this condition
        }

        // append message
        exceptionMessage.append("SSH authentication failed.");
      }
      throw new Exception(exceptionMessage.toString());
    }
  }

  //**********************************************************************
  /**
   *  disconnect Used to disconnect both the SSH and any sub-clients
   *  @throws IOException
   */
  //**********************************************************************
  public void disconnect() throws IOException
  {
    if(this.sftp != null)
    {
      this.sftp.quit();
    }

    if (this.session != null)
    {
      this.session.close();
    }

    if (this.ssh != null)
    {
      this.ssh.disconnect();
    }
  }

  //**********************************************************************
  /**
   *  setDefaultLogging Used to disable or enable the default logging
   *  mechanism (java.util.logging).
   *  @param boolean setToOn
   */
  //**********************************************************************
  public void setDefaultLogging(boolean setToOn)
  {
    Logger.getLogger("com.sshtools").setLevel(Level.OFF);
  }

  /*                   Attribute access methods                         */

  //**********************************************************************
  /**
   *  setKnownHostName This is used to set the know host attribute
   *  @param String knownHostName
   */
  //**********************************************************************
  public void setKnownHostName(String knownHostName)
  {
    this.knownHostName = knownHostName;
  }

  //**********************************************************************
  /**
   *  setKnownIPAddress This is used to set the known IP address attribute
   *  @param String knownIPAddress
   */
  //**********************************************************************
  public void setKnownIPAddress(String knownIPAddress)
  {
    this.knownIPAddress = knownIPAddress;
  }

  //**********************************************************************
  /**
   *  setKnownKey This is used to set the know key fingerprint attribute
   *  @param String knownKey
   */
  //**********************************************************************
  public void setKnownKeyFingerprint(String knownKeyFingerprint)
  {
    this.knownKeyFingerprint = knownKeyFingerprint;
  }

  //**********************************************************************
  /**
   *  setVerbose Used to set a flag indicating debug info is displayed
   *  @param boolean setting
   */
  //**********************************************************************
  public void setVerbose(boolean setting)
  {
    this.verbose = setting;
  }

  //**********************************************************************
  /**
   *  getCommandError This is used to get the commandError attribute
   *  @return String
   */
  //**********************************************************************
  public String getCommandError()
  {
    return this.commandError;
  }

  //**********************************************************************
  /**
   *  getCommandResult This is used to get the commandResult attribute
   *  @return String
   */
  //**********************************************************************
  public String getCommandResult()
  {
    return this.commandResult;
  }

  //**********************************************************************
  /**
   *  getKnownHostName This is used to get the know host attribute
   *  @return String knownHostName
   */
  //**********************************************************************
  public String getKnownHostName(String knownHostName)
  {
    return this.knownHostName;
  }

  //**********************************************************************
  /**
   *  getKnownIPAddress This is used to get the known IP address attribute
   *  @return String knownIPAddress
   */
  //**********************************************************************
  public String getKnownIPAddress()
  {
    return this.knownIPAddress;
  }

  //**********************************************************************
  /**
   *  getKnownKey This is used to get the know key fingerprint attribute
   *  @return String knownKey
   */
  //**********************************************************************
  public String getKnownKeyFingerprint()
  {
    return this.knownKeyFingerprint;
  }

  //**********************************************************************
  /**
   *  getVerbose Used to get a flag indicating debug info is displayed
   *  @return boolean verbose
   */
  //**********************************************************************
  public boolean getVerbose()
  {
    return this.verbose;
  }

  //**********************************************************************
  /**
   *  getSshClient This is used to get the SshClient attribute
   *  @return SshClient
   */
  //**********************************************************************
  public SshClient getSshClient()
  {
    return this.ssh;
  }

  //**********************************************************************
  /**
   *  getSFTPHelper This is used to get the SSHHelper attribute
   *  @return SSHHelper
   */
  //**********************************************************************
  public SSHHelper getSSHHelper()
  {
    return this.helper;
  }
}

