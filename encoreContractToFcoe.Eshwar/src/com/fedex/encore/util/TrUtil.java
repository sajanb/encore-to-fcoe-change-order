/**


*/
package com.fedex.encore.util;

import java.io.*;
import java.math.BigDecimal;
import javax.mail.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.text.SimpleDateFormat;

public class TrUtil {

  public static boolean nullBlankEmpty(String in) {
    boolean b = false;
    if(in == null) {
      b = true;
    }else {
      in = in.trim();
      if(in.equals("")) {b = true;}
    }
    return b;
  }

  public static void closeDbConn(Connection conn) throws SQLException {
    if(conn != null) {
      if(!conn.isClosed()) {
      conn.close();
      }
    }
  }


  public static void closeDbResultSet(ResultSet resultset) throws SQLException {
    if(resultset != null) {
      resultset.close();
    }
  }

  public static String getStack(Throwable t)  {
    String sout = "";
    StringWriter sw = new StringWriter();
    PrintWriter pw = new PrintWriter(sw);
    t.printStackTrace(pw);
    sout =  sw.toString();
    pw.close();
    try {
      sw.close();
    } catch(Exception exc) {}
    return sout;
  }

  public static void sendEmail(String body,String subject,String emailto,
                               String emailcc,String smtphost,String sender) throws Exception {
//System.out.println("from:" + sender + "\nto:" + emailto +"\ncc:" + emailcc +"\nsubject" + subject +"\n\n" +body + "\n host:" + smtphost);
	String emailType = "smtp";
    FX_Mail fxMail = new FX_Mail(smtphost, sender);
    StringBuffer mailMsg = new StringBuffer("");
    mailMsg.append(body);
    fxMail.sendMail(emailto, subject, mailMsg.toString(), emailType);
  }

  public static Date getDateFromFmt(String dateString,String fmt) {
    Date rDate = null;
    SimpleDateFormat df = new SimpleDateFormat(fmt);
    try {
      rDate = df.parse(dateString);
    }catch(Exception e) {
      rDate = null;
    }
    return rDate;
  }

  public static String cleanNbrString(String in) {
    StringBuffer sb = new StringBuffer();
    in = nullCheck(in);
    char c=0;
    char minus=45;
    char dot=46;
    for(int x=0; x<in.length(); x++) {
      c=in.charAt(x);
      if((c==minus) || (c==dot) || Character.isDigit(c)) {
        sb.append(c);
      }
    }
    return sb.toString();
  }

//helper for padding a String value with whatever u want
  public static String padString(String in,String side,int width,String padval) {
    String sout = "";
    if(in == null) {in = "";}
    int i = in.length();
    if(i >= width || width > 1024){return in;}

    String pad = "";
    for(int x = 0; x < (width - i); x++) {
      pad += padval;
    }

    if(side.toUpperCase().equals("LEFT")) {
      sout = pad + in;
    }else {
      sout = in + pad;
    }
    return sout;
  }

// helper for making a formatted number String (with leading zeroes)
// into a negative value ... preserving the String length. For use with
// fixed field width stuff (after padString has added a bunch of leading zeroes)
// like this
// in:   000000000000000000005263.41
// out:  -00000000000000000005263.41
  static String makeNumberStringNegative(String in) {
    String out="";
    StringBuffer sb = new StringBuffer();
    char c=0;
    if(in==null) {in="";}

    if(in.startsWith("0")) {
      for(int x=0; x<in.length(); x++) {
        c=in.charAt(x);
        if(x==0) {sb.append("-");}else {sb.append(c);}
      }
    }

    return sb.toString();
  }

  // handle positive and negative amounts while maintaining consistent field width
  //  5000 000000000000000000005000.000
  // -5000 -00000000000000000005000.000
  public static String formatAmount(BigDecimal bd, int width) {
    String amtStr="";
    if(bd.doubleValue()<0) {
      amtStr="-"+TrUtil.padString(bd.abs().toString(),"left",(width-1),"0");
    }else {
      amtStr=TrUtil.padString(bd.toString(),"left",width,"0");
    }
    return amtStr;
  }

// add more formats to the sdfs array with care as
// you probably dont want to try for example dd/MM/yyyy and
// MM/dd/yyyy ... lest the Dates be dicey as well as the formats ;)
  public static Date parseDiceyDateStrings(String in) {

    Date dt = null;
    SimpleDateFormat[] sdfs = {
      new SimpleDateFormat("MM/dd/yyyy"),
      new SimpleDateFormat("dd-MMM-yy")
    };

    for(int x=0; x<sdfs.length; x++) {
      try {
        dt=sdfs[x].parse(in);
      }catch(Exception e){}
      if(dt!=null) {break;}
    }

    return dt;
  }

  private static String nullCheck(String str){
    return str==null ? "" : str;
  }

}
