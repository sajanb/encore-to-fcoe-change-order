package com.fedex.encore.util;

/*
 * 01/2012
 * This is a shameless blatant ripoff of HRTs AesEncryption
 * class and supporting classes. I spun my own, but theirs
 * works better than mine  
 *
*/


import java.io.DataInputStream;
import java.io.UnsupportedEncodingException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

// these work, but compiler gives a proprietary warning
//import sun.misc.BASE64Decoder;
//import sun.misc.BASE64Encoder;

import org.apache.commons.codec.binary.Base64; 

public class AesEncr {
  private boolean debug = true;
  private final String DEFAULT_CHAR_ENCODING = "UTF8";
  private final String AES_TYPE = "AES";
  private final int NO_ENCODING = 0;
  private final int HEX_ENCODING = 1;
  private final int B64_ENCODING = 2;
  private final int AES_IV_PARAM_SIZE = 16;
  private final int AES_KEY_SIZE_BYTES = 32;
  private final String AES_CIPHER_TYPE = "AES/CBC/PKCS5Padding";
  
  private final char[] HEX = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};

  private IvParameterSpec ivParameterSpec;
  private Cipher encryptCipher;
  private Cipher decryptCipher;
  private Key key = null;
  
  
  public String enCrypt(String sRecord)
	throws NoSuchAlgorithmException,
		NoSuchPaddingException, InvalidKeyException,
		UnsupportedEncodingException, IllegalBlockSizeException,
		BadPaddingException, Exception {
	return enCrypt(B64_ENCODING, sRecord);
  }

  public String enCrypt(int encodingMethod, String sRecord)
	throws NoSuchAlgorithmException,
		NoSuchPaddingException, InvalidKeyException,
		UnsupportedEncodingException, IllegalBlockSizeException,
		BadPaddingException, Exception {
    String result = null;

	//
	// Encrypt the input string
	//
	byte[] stringBytes = sRecord.getBytes(DEFAULT_CHAR_ENCODING);
	byte[] raw = encryptCipher.doFinal(stringBytes);
	if (debug) {
	  msg("Encrypt raw: " + raw);
	}
	result = encode(encodingMethod, raw);
	if(debug) {
	  msg("Encoded raw: " + result);
	}
	return result;
  }

  public String deCrypt(String sRecord) throws Exception {
	return deCrypt(B64_ENCODING, sRecord);
  }

  public String deCrypt(int encodingMethod, String sRecord)	throws Exception {
	String result = null;
 // Decrypt the input string

	byte[] raw = decode(encodingMethod, sRecord);
	if(debug) {
	  msg("Decoded raw: " + raw);
	}
	byte[] stringBytes = getDecryptCipher().doFinal(raw);
	result = new String(stringBytes, DEFAULT_CHAR_ENCODING);
	if (debug) {
	  msg("Decrypted raw: " + raw);
	}
	return result;
  }
  
  public String encode(int encodingMethod, byte[] data) throws Exception {
  	String result = null;
  	if(encodingMethod == B64_ENCODING) {
  	//BASE64Encoder encoder = new BASE64Encoder();
  	//result = encoder.encode(data);
      byte[] resultBytes = Base64.encodeBase64(data); // org.apache.commons.codec.binary.Base64 
      result = new String(resultBytes);
  	}else if(encodingMethod == HEX_ENCODING) {
  	  result = encodeToString(data);
  	}else if(encodingMethod == NO_ENCODING) {
  	  throw new UnsupportedEncodingException("Invalid encoding method. NULL encoding not supported");
  	}else {
  	  throw new Exception("Invalid encoding method.");
  	}
  	return result;
  }

// Convert a byte array to a string of hex chars
  public String encodeToString(byte[] b) {
    char[] buf = new char[b.length * 2];
    int j = 0;
    int k;

    for(int i = 0; i < b.length; i++) {
      k = b[i];
      buf[j++] = HEX[(k >>> 4) & 0x0F];
      buf[j++] = HEX[ k        & 0x0F];
    }
    return new String(buf);
  } 

  public static byte[] encodeFromString(String hex) {
    if(hex == null) {
      return null;
    }

 // get rid of any trailing spaces that could mess things up
    hex = hex.trim();
    int len = hex.length();
    byte[] buf = new byte[((len + 1) / 2)];

    int i = 0, j = 0;
    if((len % 2) == 1) {
      buf[j++] = (byte) fromDigit(hex.charAt(i++));
    }

    while(i < len) {
      buf[j++] = (byte) ((fromDigit(hex.charAt(i++)) << 4) | fromDigit(hex.charAt(i++)));
    }
    return buf;
  }
  
  public byte[] decode(int encodingMethod, String data) throws Exception {
  	byte[] result = null;
  	if(encodingMethod == B64_ENCODING) {
  	//BASE64Decoder decoder = new BASE64Decoder();
  	//result = decoder.decodeBuffer(data);
  	  result = Base64.decodeBase64(data.getBytes()); // org.apache.commons.codec.binary.Base64
  	}else if(encodingMethod == HEX_ENCODING) {
  	  result = encodeFromString(data);
  	}else if(encodingMethod == NO_ENCODING) {
  	  throw new UnsupportedEncodingException("Invalid encoding method. NULL encoding not supported");
  	}else {
  	  throw new Exception("Invalid encoding method.");
  	}
  	return result;
  }
  
// Constructor
  public AesEncr(String keyFilename) throws Exception {
    InputStream fileStrm = null;
	try	{
	  if(debug) {
		msg("Reading key info");
	  }

	  fileStrm = Thread.currentThread().getContextClassLoader().getResourceAsStream(keyFilename);
	  if(fileStrm != null) {
		readKeyIvFromResource(keyFilename);
		if(debug) {
		  msg("getIvParameterSpec().getIV() after readKeyIvFromResource: " + getIvParameterSpec().getIV());
		  msg("getIvParameterSpec().getIV().length after readKeyIvFromResource: " + getIvParameterSpec().getIV().length);
		  msg("getKey().getEncoded() after readKeyIvFromResource: " + getKey().getEncoded());
		  msg("getKey().getEncoded().length after readKeyIvFromResource: " + getKey().getEncoded().length);
		  msg("calling configureCipher in AesEncryption(String keyFilename, String ivFilename, int keyFormat) for input");
		}
			configureCipher(false);
      }else {
        if(debug) {
	      msg("key not found");
		}
	 // key file should exist in a jar file no support for writing key in this mode.
		throw new UnsupportedOperationException("Key file does not exist as a resource. Writing of key is unsupported in this setup.");
      }
	}catch(Exception e)	{
	  StringBuffer strBuf = new StringBuffer();
	  strBuf.append("AesEncr constructor(String): exception encountered during object creation.\n");
	  throw new Exception(strBuf.toString(), e);
	}finally {
      if(fileStrm != null) {
		fileStrm.close();
		fileStrm = null;
	  }
	}
 }
 
  private void readKeyIvFromResource(String ivFile) throws Exception {
	IvParameterSpec iv = null;
	SecretKey key = null;
	DataInputStream in = null;
	try {
      if(ivFile != null) {
	    in = new DataInputStream(Thread.currentThread().getContextClassLoader().getResourceAsStream(ivFile));
	    byte[] rawIv = new byte[AES_IV_PARAM_SIZE];
		byte[] rawKey = new byte[AES_KEY_SIZE_BYTES];
	    in.readFully(rawIv);
		in.readFully(rawKey);
		iv = new IvParameterSpec(rawIv);
		setIvParameterSpec(iv);
		key = new SecretKeySpec(rawKey, AES_TYPE);
		setKey(key);
      }
    }finally {
	  if(in != null) {
		in.close();
		in = null;
	  }
	}
  }
  
  public void configureCipher(boolean useEncryptCipherIv) throws Exception {
    if(debug) {
	  System.out.println("In configureCipher()");
    }
	Cipher enCipher = Cipher.getInstance(AES_CIPHER_TYPE);
	Cipher deCipher = Cipher.getInstance(AES_CIPHER_TYPE);
	IvParameterSpec iv = null;
	if(useEncryptCipherIv) {
	  SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
	  random.setSeed(System.currentTimeMillis());
	  enCipher.init(Cipher.ENCRYPT_MODE, getKey(), random);
	  iv = new IvParameterSpec(enCipher.getIV());
	  setIvParameterSpec(iv);
    }else {
	  enCipher.init(Cipher.ENCRYPT_MODE, getKey(), getIvParameterSpec());
    }
    setEncryptCipher(enCipher);
	deCipher.init(Cipher.DECRYPT_MODE, getKey(), getIvParameterSpec());
	setDecryptCipher(deCipher);
  }

  public static int fromDigit(char ch) {
    if(ch >= '0' && ch <= '9') {
      return ch - '0';
    }

    if(ch >= 'A' && ch <= 'F') {
      return ch - 'A' + 10;
    }

    if(ch >= 'a' && ch <= 'f') {
      return ch - 'a' + 10;
    }
    throw new RuntimeException("Invalid hex digit '" + ch + "'");
  }
  
  public void setKey(Key newKey) {
	key = newKey;
  }
  
  public void setIvParameterSpec(IvParameterSpec ivParameterSpec) {
	this.ivParameterSpec = ivParameterSpec;
  }

  public void setEncryptCipher(Cipher encryptCipher) {
	this.encryptCipher = encryptCipher;
  }
  
  private void setDecryptCipher(Cipher decryptCipher) {
	this.decryptCipher = decryptCipher;
  }
  
  public IvParameterSpec getIvParameterSpec() {
	return ivParameterSpec;
  }

  public Key getKey() {
	return key;
  }

  private Cipher getDecryptCipher() {
	return decryptCipher;
  }
  
  private void msg(Object o) {
	System.out.println(""+o);  
  }
  
}
