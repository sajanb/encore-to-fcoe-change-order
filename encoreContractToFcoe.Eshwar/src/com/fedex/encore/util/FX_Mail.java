package com.fedex.encore.util;

import java.util.*;
import java.io.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

// import com.fedex.hrt.commonclasses.exceptionclasses.MailException;


 /**
  * Class FX_Mail
  *
  *     This class provides     a generic mail class wrapper
  *      which uses the javax.mail classes.
  *      @author William Wixon, revised by Keith Dodge
  *      @version $Revision: 1.1 $
  */
 public class FX_Mail
 {

     /** Field smtpHost           */
     private String smtpHost = null;

     /** Field smtpPort           */
     private String smtpPort = null;

     /** Field mailUser           */
     private String mailUser = null;

     /** Field mailSession           */
     private Session mailSession = null;


     public static void main(String[] args)
     {
         if(args.length != 7 && args.length != 5 )
         {
             System.err.println("Usage:  " + FX_Mail.class.getName() +
                                " serverName fromAddress subject toAddresses " +
                                "message [attachedFile localFilename]");
             System.exit(2);
         }

         FX_Mail mailer = new FX_Mail(args[0], args[1]);

         try
         {
            if (args.length == 7)
            {
                 mailer.sendMail(args[3], args[2], args[4],
                             "smtp", args[5], args[6]);
            }
            else
            {
                 mailer.sendMail(args[3], args[2], args[4],
                             "smtp");
            }
         }
         catch(Exception e)
         {
             e.printStackTrace();
             System.exit(1);
         }
     }

     /***********************************************************************
      * Initializes the object with the appropriate values for creating
      *  the mail Session object.  This constructor builds the     session
      *  object with the host and user     provided for smtp mail servers.
      *  @param host the host mail server to use to send the mail
      *  @param user the from user of the email
      */
     // **********************************************************************
     public FX_Mail(String host, String user)
     {

         Properties props = new Properties();

         /* Sets the host and user in the properties object. */
         props.put("mail.smtp.host", host);
         props.put("mail.smtp.user", user);

         /* Sets the smtpHost and mailUser in the class     variables. */
         this.smtpHost    = host;
         this.mailUser    = user;
         this.mailSession = Session.getDefaultInstance(props, null);
       //this.mailSession.setDebug(true);
     }

     /**********************************************************************
      * Void method that sends the email.
      *  exception instance.
      *  @param To To user of the email;This can be one email
      *            address or a list of email address'.
      *            To specify a list, send it in the form of a String with
      *            comma delimeters. (e.g. wcwixon@fedex.com,smbyers@fedex.com)
      *  @param Subject         the subject of the email
      *  @param mailText        the message text of the email
      *  @param transportType   the method of tranporting the email(smtp)
      *  @throws MailException  if any error occurs
      */
     // *************************************************************
     public void sendMail(String To, String Subject, String mailText,
                          String transportType)
          throws Exception
     {

         Message mailMessage = buildMessageHead(To, Subject, null,
                                                null);

         sendTextMessage(mailMessage, mailText, transportType);
     }                            // sendMail     method (no attch)

     /**
      * Method sendMail
      *
      *
      * @param To
      * @param ccList
      * @param Subject
      * @param mailText
      * @param transportType
      *
      * @throws MailException
      *
      */
     public void sendMail(String To, String[] ccList, String Subject,
                          String mailText, String transportType)
          throws Exception
     {

         String cc  = null;
         String bcc = null;

         if ((ccList != null) && (ccList.length > 0)
                 && (ccList[0] != null))
         {
             cc = ccList[0];
         }

         if ((ccList != null) && (ccList.length > 1)
                 && (ccList[1] != null))
         {
             bcc = ccList[1];
         }

         Message mailMessage = buildMessageHead(To, Subject, cc, bcc);

         sendTextMessage(mailMessage, mailText, transportType);
     }

     // *************************************************************
     /**
      * Void method that sends the email with     an attachment.
      *  exception instance.
      *  @param To To user of the email;This can be one email
      *            address or a list of email address'.
      *            To specify a list, send it in the form of a String with
      *            comma delimeters. (e.g. wcwixon@fedex.com,smbyers@fedex.com)
      *  @param Subject         the subject of the email
      *  @param mailText        the message text of the email
      *  @param transportType   the method of tranporting the email(smtp)
      *  @param fileAndPathToAttach
      *  @param fileNameWhenAttached
      *  @throws MailException     if any error occurs
      */
     // *************************************************************
     public void sendMail(String To, String Subject, String mailText,
                          String transportType,
                          String fileAndPathToAttach,
                          String fileNameWhenAttached)
          throws Exception
     {

         Message mailMessage = buildMessageHead(To, Subject, null,
                                                null);

         sendMultipartMessage(mailMessage, transportType, mailText,
                              fileAndPathToAttach,
                              fileNameWhenAttached);
     }                            // sendMail     method (attch)


     // *************************************************************
     /**
      * Void method that sends the email with multiple attachments
      *  @param To To user of the email;This can be one email
      *            address or a list of email address'.
      *            To specify a list, send it in the form of a String with
      *            comma delimeters. (e.g. wcwixon@fedex.com,smbyers@fedex.com)
      *  @param Subject         the subject of the email
      *  @param mailText        the message text of the email
      *  @param transportType   the method of tranporting the email(smtp)
      *  @param attachments     a hashtable of attachment names, file specifications
      *  @throws MailException     if any error occurs
      */
     // *************************************************************
     public void sendMail(String To, String Subject, String mailText,
                          String transportType,
                          Hashtable attachments)
          throws Exception
     {

         Message mailMessage = buildMessageHead(To, Subject, null,
                                                null);

         sendMultipartMessage(mailMessage, transportType, mailText,
                              attachments);
     }    // sendMail     method (Hashtable attachments)

     /**
      * Method sendMail
      *
      *
      * @param To
      * @param ccList
      * @param Subject
      * @param mailText
      * @param transportType
      * @param fileAndPathToAttach
      * @param fileNameWhenAttached
      *
      * @throws MailException
      *
      */
     public void sendMail(String To, String[] ccList, String Subject,
                          String mailText, String transportType,
                          String fileAndPathToAttach,
                          String fileNameWhenAttached)
          throws Exception
     {

         String cc  = null;
         String bcc = null;

         if ((ccList != null) && (ccList.length > 0)
                 && (ccList[0] != null))
         {
             cc = ccList[0];
         }

         if ((ccList != null) && (ccList.length > 1)
                 && (ccList[1] != null))
         {
             bcc = ccList[1];
         }

         Message mailMessage = buildMessageHead(To, Subject, cc, bcc);

         sendMultipartMessage(mailMessage, transportType, mailText,
                              fileAndPathToAttach,
                              fileNameWhenAttached);
     }

     /**
      * Method sendMail
      *
      *
      *  @param To To user of the email;This can be one email
      *            address or a list of email address'.
      *            To specify a list, send it in the form of a String with
      *            comma delimeters. (e.g. wcwixon@fedex.com,smbyers@fedex.com)

      *  @param ccList          array of mail ids to copy
      *  @param Subject         the subject of the email
      *  @param mailText        the message text of the email
      *  @param transportType   the method of tranporting the email(smtp)
      *  @param attachments     a hashtable of attachment names, file specifications
      *  @throws MailException     if any error occurs
      */
     public void sendMail(String To, String[] ccList, String Subject,
                          String mailText, String transportType,
                          Hashtable attachments)
          throws Exception
     {

         String cc  = null;
         String bcc = null;

         if ((ccList != null) && (ccList.length > 0)
                 && (ccList[0] != null))
         {
             cc = ccList[0];
         }

         if ((ccList != null) && (ccList.length > 1)
                 && (ccList[1] != null))
         {
             bcc = ccList[1];
         }

         Message mailMessage = buildMessageHead(To, Subject, cc, bcc);

         sendMultipartMessage(mailMessage, transportType, mailText,
                              attachments);
     }


     //*************************************************************
     /** Void method that sends the email with html display.
      *  exception instance.
      *  @param To the To user of the email;This can be one email
      *              address or a list of email address'.  To specify
      *              a list, send it in the form of a String with
      *              comma delimeters. ex:wcwixon@fedex.com,smbyers@fedex.com........
      *  @param Subject the subject of the email
      *  @param mailText the message text of the email
      *  @param transportType the method of tranporting the email(smtp)
      *  @throws MailException if any error occurs
      */
     //*************************************************************
     public void sendMail(String To, String Subject, StringBuffer mailBody, String transportType) throws Exception
     {
         Message mailMessage = buildMessageHead(To, Subject, null, null);
         sendHtmlMessage(mailMessage, mailBody, transportType);
     }

     /*************************************************************
       * Method sendMail
       * @param To
       * @param ccList
       * @param Subject
       * @param mailText
       * @param transportType
       *
       * @throws MailException
       *************************************************************
      */

     public void sendMail(String To, String[] ccList, String Subject, StringBuffer mailBody, String transportType) throws Exception
     {
         String cc  = null;
         String bcc = null;

         if ((ccList != null) && (ccList.length > 0) && (ccList[0] != null))
         {
             cc = ccList[0];
         }

         if ((ccList != null) && (ccList.length > 1) && (ccList[1] != null))
         {
             bcc = ccList[1];
         }

         Message mailMessage = buildMessageHead(To, Subject, cc, bcc);
         sendHtmlMessage(mailMessage, mailBody, transportType);
     }


     /**
      * Method that builds the message and sets its header.
      *  @param To To user of the email;This can be one email
      *            address or a list of email address'.
      *            To specify a list, send it in the form of a String with
      *            comma delimeters. (e.g. wcwixon@fedex.com,smbyers@fedex.com)
      *  @param ccList  any email addresses to be CC-ed
      *  @param bccList any email addresses to be BCC-ed
      *  @param Subject the subject of the email
      *
      *  @return MimeMessage the built message
      *  @throws MailException     if any error occurs
      */

     // *************************************************************
     private MimeMessage buildMessageHead(String To, String Subject,
                                          String ccList, String bccList)
          throws Exception
     {

         InternetAddress[] mailTo      = null;
         InternetAddress[] mailFrom    = null;
         InternetAddress[] mailCC      = null;
         InternetAddress[] mailBCC     = null;
         InternetAddress   mailList    = new InternetAddress();
         MimeMessage       mailMessage =
             new MimeMessage(this.mailSession);

         if (To == null)
         {
             throw new Exception(
                 "TO address was null, unable to send mail");
         }

         try
         {

             /* Parses the String     arg     'To' into an InternetAddress object. */
             mailTo   = mailList.parse(To);
             mailFrom = mailList.parse(this.mailUser);
         }
         catch (AddressException ae)
         {
             throw new Exception(
                 "AddressException on InternetAddress parse(): "
                 + ae.getMessage());
         }

         try
         {

             /* Sets who to mail to and their RecipientType (TO,CC,BCC). */
             mailMessage.setRecipients(Message.RecipientType.TO,
                                       mailTo);
         }
         catch (MessagingException me)
         {
             throw new Exception(
                 "MessagingException     on InternetAddress setRecipients():     "
                 + me.getMessage());
         }

         if (ccList != null)
         {
             try
             {

                 /* Parses     the     String arg into an InternetAddress object. */
                 mailCC = mailList.parse(ccList);
             }
             catch (AddressException ae)
             {
                 throw new Exception(
                     "AddressException on InternetAddress parse(CC): "
                     + ae.getMessage());
             }

             try
             {

                 /* Sets who to mail to and their      RecipientType (TO,CC,BCC). */
                 mailMessage.setRecipients(Message.RecipientType.CC,
                                           mailCC);
             }
             catch (MessagingException me)
             {
                 throw new Exception(
                     "MessagingException  on InternetAddress setRecipients(CC):   "
                     + me.getMessage());
             }
         }

         if (bccList != null)
         {
             try
             {

                 /* Parses     the     String arg into an InternetAddress object. */
                 mailBCC = mailList.parse(bccList);
             }
             catch (AddressException ae)
             {
                 throw new Exception(
                     "AddressException on InternetAddress parse(BCC): "
                     + ae.getMessage());
             }

             try
             {

                 /* Sets who to mail to and their      RecipientType (TO,CC,BCC). */
                 mailMessage.setRecipients(Message.RecipientType.BCC,
                                           mailBCC);
             }
             catch (MessagingException me)
             {
                 throw new Exception(
                     "MessagingException  on InternetAddress setRecipients(BCC):  "
                     + me.getMessage());
             }
         }

         try
         {

             /* Sets who the message is from(gets     it from properties object). */
             mailMessage.setFrom(mailFrom[0]);
         }
         catch (MessagingException me)
         {
             throw new Exception(
                 "MessagingException     on message setFrom(): "
                 + me.getMessage());
         }

         try
         {

             /* Sets the subject of the email. */
             mailMessage.setSubject(Subject);
         }
         catch (MessagingException me)
         {
             throw new Exception(
                 "MessagingException     on message setSubject(): "
                 + me.getMessage());
         }

         Calendar c = Calendar.getInstance();

         try
         {

             /* Sets the date and     time of the     email . */
             mailMessage.setSentDate(c.getTime());
         }
         catch (MessagingException me)
         {
             throw new Exception(
                 "MessagingException     on InternetAddress setSentDate(): "
                 + me.getMessage());
         }

         return mailMessage;
     }

     /**
      * Method that sends the     message as text only.
      *
      * @param mailMessage
      *  @param mailText the message text of the email
      *  @param transportType the method of tranporting the email(smtp)
      *  @throws MailException     if any error occurs
      */

     // *************************************************************
     private void sendTextMessage(Message mailMessage, String mailText,
                                  String transportType)
          throws Exception
     {

         try
         {

             /* Sets the email message text. */
             mailMessage.setText(mailText);
         }
         catch (MessagingException me)
         {
             throw new Exception(
                 "MessagingException     on InternetAddress setText(): "
                 + me.getMessage());
         }

         Transport mailer = null;

         try
         {

             /* Gets a Transport object from the session object. */
             mailer = this.mailSession.getTransport(transportType);
         }
         catch (NoSuchProviderException nsp)
         {
             throw new Exception(
                 "NoSuchProviderException on     Transport Session.getTransport(): "
                 + nsp.getMessage());
         }

         try
         {

             /* Connects the transport object to the mail     server. */
             mailer.connect(this.smtpHost, this.mailUser, null);
         }
         catch (MessagingException me)
         {
             throw new Exception(
                 "MessagingException     on Transport connect(): "
                 + me.getMessage());
         }

         try
         {

             /* Sends the     message. */
             mailer.sendMessage(mailMessage,
                                mailMessage.getAllRecipients());
         }
         catch (SendFailedException sfe)
         {
             throw new Exception(
                 "SendFailedException on Transport sendMessage(): "
                 + sfe.getMessage());
         }
         catch (MessagingException me)
         {
             throw new Exception(
                 "MessagingException     on Transport sendMessage():     "
                 + me.getMessage());
         }
     }    // sendTextMessage  method

     /**
      * Method that sends the     message as multipart mime message.
      *
      * @param mailMessage
      *  @param mailText the message text of the email
      *  @param transportType the method of tranporting the email(smtp)
      *  @param fileAndPathToAttach path and file to a single file to attach
      *  @param fileNameWhenAttached name of file once attached to email
      *  @throws MailException if any error occurs
      */

     // *************************************************************
     private void sendMultipartMessage(Message mailMessage,
                                       String transportType,
                                       String mailText,
                                       String fileAndPathToAttach,
                                       String fileNameWhenAttached)
          throws Exception
     {

         Hashtable<String,String> attachments = new Hashtable<String,String>();
         attachments.put(fileNameWhenAttached, fileAndPathToAttach);

         sendMultipartMessage(mailMessage, transportType, mailText, attachments);

     }    // sendMultipartMessage method




     /**
      * Method that sends the  message as multipart mime message.
      *
      * @param mailMessage
      *  @param mailText the message text of the email
      *  @param transportType the method of tranporting the email(smtp)
      *  @param attachments   a hashtable of filename
      *  @throws MailException if any error occurs
      */

     // *************************************************************
     private void sendMultipartMessage(Message mailMessage,
                                       String transportType,
                                       String mailText,
                                       Hashtable attachments)
          throws Exception
     {

         // create     and     fill the first message part
         MimeBodyPart mbp1 = new MimeBodyPart();

         try
         {

             /* Sets the email message text. */
             mbp1.setText(mailText);
         }
         catch (MessagingException me)
         {
             throw new Exception(
                 "MessagingException     on InternetAddress setText(): "
                 + me.getMessage());
         }


         // create     the     Multipart and its parts to it
         Multipart mp = new MimeMultipart();

         try
         {
             mp.addBodyPart(mbp1);
         }
         catch (MessagingException me)
         {
             throw new Exception(
                 "MessagingException     on InternetAddress addBodyPart(mbp1): "
                 + me.getMessage());
         }

         // add each attachment to the Multipart
         try
         {
             String attachName = null;
             String attachFile = null;

             Enumeration enum1 = attachments.keys();

             while (enum1.hasMoreElements())
             {
                 attachName = (String) enum1.nextElement();
                 attachFile = (String) attachments.get(attachName);
                 //System.out.println("key=["+attachName+"]   data=["+attachFile+"]");
                 MimeBodyPart attch = createAttachmentPart(attachName, attachFile);
                 mp.addBodyPart(attch);
             }

         }
         catch (MessagingException me)
         {
             throw new Exception( "Excpetion on multiple attachment " +
                                      me.getMessage());
         }

         try
         {

             // add the Multipart to the message
             mailMessage.setContent(mp);
         }
         catch (MessagingException me)
         {
             throw new Exception(
                 "MessagingException     on InternetAddress setContent(): "
                 + me.getMessage());
         }

         Transport mailer = null;

         try
         {

             /* Gets a Transport object from the session object. */
             mailer = this.mailSession.getTransport(transportType);
         }
         catch (NoSuchProviderException nsp)
         {
             throw new Exception(
                 "NoSuchProviderException on     Transport Session.getTransport("
                 + transportType + "): " + nsp.getMessage());
         }

         try
         {

             /* Connects the transport object to the mail     server. */
             mailer.connect(this.smtpHost, this.mailUser, null);
         }
         catch (MessagingException me)
         {
             throw new Exception(
                 "MessagingException     on Transport connect(): "
                 + me.getMessage());
         }

         try
         {

             /* Sends the     message. */
             mailer.send(mailMessage);
         }
         catch (SendFailedException sfe)
         {
             throw new Exception(
                 "SendFailedException on Transport sendMessage(): "
                 + sfe.getMessage());
         }
         catch (MessagingException me)
         {
             throw new Exception(
                 "MessagingException     on Transport sendMessage():     "
                 + me.getMessage());
         }
     }    // sendMultipartMessage method


     ////add the method to sent html screen in the email
     //*************************************************************
     /** Void method that sends the email with html display.
      *  exception instance.
      *  @param To the To user of the email;This can be one email
      *              address or a list of email address'.  To specify
      *              a list, send it in the form of a String with
      *              comma delimeters. ex:wcwixon@fedex.com,smbyers@fedex.com........
      *  @param Subject the subject of the email
      *  @param mailText the message text of the email
      *  @param transportType the method of tranporting the email(smtp)
      *  @throws MailException if any error occurs
      */
     //*************************************************************
     public void sendHtmlMessage(Message mailMessage, StringBuffer mailBody, String transportType) throws Exception
     {
        // create and fill the first message part
        MimeBodyPart mb1 = new MimeBodyPart();
        try
        {
            /*Sets the email message text.*/
             mb1.setContent(mailBody.toString(), "text/html");
        }
        catch(MessagingException me)
        {
            throw new Exception("MessagingException on InternetAddress setText(): "+ me.getMessage());
        }

        // create the Multipart and its parts to it
        MimeMultipart mp = new MimeMultipart();

        try
        {
            mp.addBodyPart(mb1);
        }
        catch(MessagingException me)
        {
            throw new Exception("MessagingException on InternetAddress addBodyPart(mbp1): "+ me.getMessage());
        }

        try
        {
            // add the Multipart to the message
            mailMessage.setContent(mp);
        }
        catch(MessagingException me)
        {
           throw new Exception("MessagingException on InternetAddress setContent(): "+ me.getMessage());
        }

        Transport mailer = null;

        try
        {
            /*Gets a Transport object from the session object.*/
            mailer = mailSession.getTransport(transportType);
        }
        catch(NoSuchProviderException nsp)
        {
            throw new Exception("NoSuchProviderException on Transport Session.getTransport(): "+ nsp.getMessage());
        }

        try
        {
            /*Connects the transport object to the mail server.*/
            mailer.connect(this.smtpHost, this.mailUser, null);
        }
        catch(MessagingException me)
        {
            throw new Exception("MessagingException on Transport connect(): "+ me.getMessage());
        }

        try
        {
            /*Sends the message.*/
            mailer.send(mailMessage);
        }
        catch(SendFailedException sfe)
        {
            throw new Exception("SendFailedException on Transport sendMessage(): "+ sfe.getMessage());
        }
        catch(MessagingException me)
        {
            throw new Exception("MessagingException on Transport sendMessage(): "+ me.getMessage());
        }
    }
    ////end the adding on 05/13/2003

     /**
      * Method that sends the  message as multipart mime message.
      *
      * @param mailMessage
      *  @param mailText the message text of the email
      *  @param transportType the method of tranporting the email(smtp)
      *  @param attachments   a hashtable of filename
      *  @throws MailException if any error occurs
      */

     // *************************************************************
     private static MimeBodyPart createAttachmentPart(
                                                 String fileNameWhenAttached,
                                                 String fileAndPathToAttach)
          throws MessagingException
     {


         // create     the     second message part
         MimeBodyPart mbp = new MimeBodyPart();

         // attach     the     file to the     message
         FileDataSource fds = new FileDataSource(fileAndPathToAttach);

         try
         {
             mbp.setDataHandler(new DataHandler(fds));
         }
         catch (MessagingException me)
         {
             throw new MessagingException(
                 "MessagingException  on setDataHandler(): [ " +
                 fileAndPathToAttach + "]" + me.getMessage());
         }

         try
         {
             mbp.setFileName(fileNameWhenAttached);
         }
         catch (MessagingException me)
         {
             throw new MessagingException(
                 "MessagingException on setFileName(): [" +
                 fileNameWhenAttached + "] " +me.getMessage());
         }
         return mbp;

     }    // createAttachmentPart method

 }// class

 /*--- Formatted in FedEx HRT custom style Style on Tue, Feb 4, '03 ---*/
 /*------ Formatted by Jindent 3.24 Gold 1.02 --- http://www.jindent.de ------*/

