/**
 
 08/2013

*/
package com.fedex.encore.fcoe;

import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.Vector;
import javax.mail.*;

import java.io.*;
import java.math.BigDecimal;

import java.text.SimpleDateFormat;
import java.text.DateFormat;

import javax.crypto.SecretKey;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

// Import log4j classes.
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.fedex.encore.util.AesEncr;
import com.fedex.encore.util.GTokenizer1;
import com.fedex.encore.util.PropertyUtility;
//import com.fedex.encore.util.RowHelper;
import com.fedex.encore.util.SSHHelper;
import com.fedex.encore.util.TrUtil;
import com.sshtools.j2ssh.SftpClient;


public class FcoeOutbound {

  private static String log4jCfgFileName = "fcoeOutBoundLog4j.properties";

  static Logger logger = Logger.getLogger(FcoeOutbound.class);
  private static final String taskName = "FcoeOutbound";
  private static String env = "";
  private static String action = "";
  static final String FIELDPARAMPREFIX = "Fmt";
  static int recordLength = 0;
  
  private static boolean initProps = false;
  private static boolean initDbConn = false;
  private static boolean sftpFileSend = false;

  private static Properties props = null;
  private static Properties log4jProps = null;

  private static Connection dbconn = null;
  private static PreparedStatement pstmt1 = null;
  private static PreparedStatement pstmt2 = null;
  private static PreparedStatement pstmt3 = null;
  private static PreparedStatement pstmt4 = null;

  private static ResultSet rset1 = null;
  private static ResultSet rset2 = null;
  private static AesEncr aes = null;

  static String cfgFileName = "fcoeOutbound";
  static String cfgFileExt = "cfg";

  static String keyFileName = "g10.k";

  static String emailHost       = "";
  static String excEmailTo      = "";
  static String excEmailFrom    = "";
  static String emailSubjectExc = "";
  static String devGroupMsgs = "";
  static String emsg = "";

  static ArrayList<HdrStgBean> contractsArrLst = null;
  static ArrayList<HdrStgBean> headersArrLst = null;
  static ArrayList<ItmStgBean> lineItemsArrLst = null;
//static ArrayList<RowHelper> rowsArrLst = null;

  static char[] recordCharsArr = null;
  static char[] crlfarr = {13,10};
  private static final String CRLF = new String(crlfarr);
  
  private static String sftpSiteHost = ""; 
  private static String sftpSitePort = ""; 
  private static String sftpSiteUser = "";
  private static String sftpSitePwd = "";
  static int jReturnValue=0;

  public static void main(String[] args) {
    String st1 = "7";
    String st2 = "7";
    emsg += " test start";
    //int zzz = 2500000000;
    //int zzz = 2147483647;
    //long zzl = 9999999999999L;
    
    //if(args.length>0){action=args[0];}
    //if(args.length>1){st2=args[1];}

    try {
      init();
    }catch(Exception e) {
      emsg = "running init() caught:" + e + "\n" + getStack(e) + "\nExiting"; 
      msg(emsg);
      logger.error(emsg);
   // no use continuing
      cleanUp();
      jReturnValue=1;
      msg(new Date()+": java "+taskName+" exiting with value ["+jReturnValue+"]");
      System.exit(jReturnValue);

    }

    runMe(st1,st2);
    cleanUp();

    msg(new Date()+": java "+taskName+" exiting with value ["+jReturnValue+"]");
    //FcoeCOOutbound.main(null);
    System.exit(jReturnValue);
  }

  private static void runMe(String marg, String marg2) {

    String localHostName = "";
    String cwd = ""; 
    File fcwd = new File(".");
    java.net.InetAddress localHost = null;

    try {
      localHostName = java.net.InetAddress.getLocalHost().getCanonicalHostName();    
      cwd=fcwd.getCanonicalPath(); 
    }catch(Exception uhe) {}

    logger.info("running. host["+localHostName+"] cwd["+cwd+"]");

  //msg(new java.util.Date() + ": " + taskName + " running");
    int headersExtracted = 0;
    int lineItemsExtracted = 0;
    int recordsUpdated = 0;
    int contractNumbersExamined = 0;
    int lineItemsOnThisContract = 0;
    int nonZer0Dollarlines = 0;
    contractsArrLst = new ArrayList<HdrStgBean>();

    String localFilePath = "";
    localFilePath = props.getProperty("remoteFile");
    
 // delete for fresh start
    logger.info("deleting local output file for fresh start");
    File outputFile = new File(props.getProperty("remoteFile"));
    if(outputFile.exists()) {outputFile.delete();}
    
    int rowsForTab = 0;
    String fileRecord = "";
    try {
      pstmt1 = dbconn.prepareStatement(props.getProperty("HdrStgSql1"));
      pstmt2 = dbconn.prepareStatement(props.getProperty("ItemStgSql1"));
      pstmt3 = dbconn.prepareStatement(props.getProperty("HdrStgUpdateSql"));
      pstmt4 = dbconn.prepareStatement(props.getProperty("ItemStgUpdateSql"));

      logger.info("getting initial list of contractNumbers");
   // get the list of contractNumbers
      rset1 = pstmt1.executeQuery();
      
   // BUSINESS_UNIT,CONTRACT_NUMBER,CURRENCY,VENDOR_ID f
      String rowId="";
      BigDecimal headerHdrCtrlNumber=null;
      BigDecimal itemHdrCtrlNumber=null;
      String hdrBusinessUnit="";
      String itmBusinessUnit="";
      String contractNumber="";
      String contractNumSffx="";

      String docNumber="";
      String currency="";
      String vendorId="";
      String pcBusinessUnit="";
      String projectId="";
      
      HdrStgBean hdrStg=null;
      
      logger.info("walking rset1 (header records)");
      while(rset1.next()) {
     // clear em
        rowId="";
        headerHdrCtrlNumber=null;
        contractNumber="";
        contractNumSffx="";

        docNumber="";
        hdrBusinessUnit="";
        currency="";
        vendorId="";
        hdrStg=null;
        
        rowId = nullCheck(rset1.getString("ROWID"));
        headerHdrCtrlNumber = rset1.getBigDecimal("HDR_CTRL_NUMBER");
        contractNumber = rset1.getString("CONTRACT_NUMBER");
        contractNumSffx = nullCheck(rset1.getString("CONTRACT_NUM_SUFFIX")); // contract amendment number
      //contractNumber=contractNumber+contractNumSffx;
        
        docNumber = rset1.getString("DOC_NUMBER");
        hdrBusinessUnit = rset1.getString("BUSINESS_UNIT");
        currency = rset1.getString("CURRENCY");
        vendorId = rset1.getString("VENDOR_ID");

        hdrStg = new HdrStgBean();
        hdrStg.setRowId(rowId);
        hdrStg.setHeaderCtrlNumber(headerHdrCtrlNumber);
        hdrStg.setBusinessUnit(hdrBusinessUnit);
        hdrStg.setContractNumber(contractNumber);
        hdrStg.setDocNumber(docNumber);
        hdrStg.setCurrency(currency);
        hdrStg.setVendorId(vendorId);
        
        msg(EncoreToFCOEExtract.createHdrStgRecord(hdrBusinessUnit, contractNumber+contractNumSffx, docNumber, currency, vendorId, contractNumber+contractNumSffx, vendorId, hdrBusinessUnit, currency));
        
        contractNumbersExamined++;
        msg("adding record["+contractNumbersExamined+"] "+rowId+"|"+contractNumber+contractNumSffx+"|"+docNumber);
        contractsArrLst.add(hdrStg);
      }
      
      rset1.close();

      if(true) {
     // msg("DEBUG returning");
     // return;
      }
      
     Hashtable hrow = null; 

  // declare Strings for individual column and file record values
  // ItemStgSql1=select BUSINESS_UNIT,CONTRACT_NUMBER,LINE_NUMBER,DUE_DATE,VENDOR_ID,QUANTITY,ITEM_COST,PRE_TAX_PRE_FRE_TOTAL,CURRENCY,COST_CENTER,PRODUCT_ID from FDX_CONTRACT_ITEMS_OUT where CONTRACT_NUMBER=?
     
     itmBusinessUnit=""; // BUSINESS_UNIT
     contractNumber=""; // CONTRACT_NUMBER
     contractNumSffx=""; // CONTRACT_NUM_SUFFIX contract amendment number

     currency=""; // CURRENCY
     vendorId=""; // VENDOR_ID
   //String rowId="";
     String lineNumber=""; // LINE_NUMBER
     Date dueDate=null; // DUE_DATE
     String quantity=""; // QUANTITY 
     String moranvilleQuantity="1"; // QUANTITY 
     String moranvilleItemCost="0.00"; // ITEM_COST 
     String itemCost=""; // ITEM_COST
     BigDecimal itemCostNumber=null; // ITEM_COST
     String preTaxPreFreTotal=""; // PRE_TAX_PRE_FRE_TOTAL
     BigDecimal preTaxPreFreTotalNumber=null; // PRE_TAX_PRE_FRE_TOTAL
     BigDecimal freightAmountNumber=null;
     BigDecimal reimburseAmountNumber=null;
     BigDecimal merchandiseAmountNumber=null;
     BigDecimal[] amounts=null; 

     String costCenter=""; // COST_CENTER
     String productId; // PRODUCT_ID

     String activityId; // ACTIVITY_ID - new Jan 2014
     String resourceType; // RESOURCE_TYPE - new Jan 2014

     String account; // ACCOUNT
     String tmpString="";
     lineItemsArrLst = new ArrayList();
     headersArrLst = new ArrayList();
     HdrStgBean hsb=null; 
     ItmStgBean itsb=null; 
     
   // loop through projects found
     logger.info("walking contractsArrLst["+contractsArrLst.size()+"] contracts found");
     for(int p = 0; p < contractsArrLst.size(); p++) {
       hsb=null; // new contract. clear it
       hdrBusinessUnit="";
       
      // set it 
       hsb=contractsArrLst.get(p);
       hsb.setWrittenOut(false);
       String contractNbr2 = hsb.getContractNumber();
       BigDecimal headerHdrCtrlNumberFromHeaderBean = hsb.getHeaderCtrlNumber();
       String docNumber2 = hsb.getDocNumber(); // DOC_NUMBER from hdr
       hdrBusinessUnit = hsb.getBusinessUnit(); // hdr bu
       msg("");
       logger.info("processing contract[" +(p+1)+"] id["+ contractNbr2+"] hdrCtrlId["+hsb.getHeaderCtrlNumber()+"]"); 

       pstmt2.setObject(1,contractNbr2);
       pstmt2.setObject(2,headerHdrCtrlNumberFromHeaderBean);
       rset2=pstmt2.executeQuery();
       lineItemsOnThisContract = 0; // reset it for new contract hdr
       nonZer0Dollarlines = 0; // reset it for new contract hdr
       
       while(rset2.next()) {
         lineItemsOnThisContract++;
         //msg(contractNbr2+" hello");
      // clear em
         itsb=null; // ItmStgBean - lineItem bean
         itemHdrCtrlNumber=null; // HDR_CTRL_NUMBER
         itmBusinessUnit=""; // BUSINESS_UNIT
         pcBusinessUnit=""; // PC_BU
         projectId=""; // PROJECT_ID
         contractNumber=""; // CONTRACT_NUMBER
         contractNumSffx=""; // CONTRACT_NUM_SUFFIX contract amendment number

         currency=""; // CURRENCY
         vendorId=""; // VENDOR_ID
         rowId="";
         lineNumber=""; // LINE_NUMBER
         dueDate=null; // DUE_DATE
         quantity=""; // QUANTITY 
         itemCost=""; // ITEM_COST
         itemCostNumber=null; // ITEM_COST
         preTaxPreFreTotal=""; // PRE_TAX_PRE_FRE_TOTAL
         preTaxPreFreTotalNumber=null; // 
         freightAmountNumber=null; // FREIGHT_AMOUNT
         reimburseAmountNumber=null; // REIMBURSE_AMOUNT
         merchandiseAmountNumber=null; // sum PRE_TAX_PRE_FRE_TOTAL+FREIGHT_AMOUNT+REIMBURSE_AMOUNT
         amounts = new BigDecimal[3];

         costCenter=""; // COST_CENTER
         productId=""; // PRODUCT_ID
         account=""; // ACCOUNT

         itsb= new ItmStgBean();
         rowId=rset2.getString("ROWID");
         itemHdrCtrlNumber = rset2.getBigDecimal("HDR_CTRL_NUMBER");
         itmBusinessUnit = rset2.getString("BUSINESS_UNIT"); // BUSINESS_UNIT
         pcBusinessUnit = rset2.getString("PC_BU"); // PC_BU
         projectId = rset2.getString("PROJECT_ID"); // PROJECT_ID
         contractNumber = rset2.getString("CONTRACT_NUMBER");
         contractNumSffx = nullCheck(rset2.getString("CONTRACT_NUM_SUFFIX"));
       //contractNumber=contractNumber+contractNumSffx;

         currency = rset2.getString("CURRENCY");
         vendorId = rset2.getString("VENDOR_ID");

         lineNumber = rset2.getString("LINE_NUMBER");
         dueDate = (Date) rset2.getObject("DUE_DATE");
         quantity = rset2.getString("QUANTITY");
         itemCost = rset2.getString("ITEM_COST");
         itemCostNumber = rset2.getBigDecimal("ITEM_COST");
         preTaxPreFreTotalNumber = nullCheck(rset2.getBigDecimal("PRE_TAX_PRE_FRE_TOTAL"));
         freightAmountNumber     = nullCheck(rset2.getBigDecimal("FREIGHT_AMOUNT"));
         reimburseAmountNumber   = nullCheck(rset2.getBigDecimal("REIMBURSE_AMOUNT"));

         amounts[0]=preTaxPreFreTotalNumber;
         amounts[1]=freightAmountNumber;
         amounts[2]=reimburseAmountNumber;

         merchandiseAmountNumber = sumMyBigDecimals(amounts);
         merchandiseAmountNumber = merchandiseAmountNumber.setScale(2,BigDecimal.ROUND_HALF_UP);
         
         if(itemCostNumber==null) {
           msg("itemCostNumber==null");
           itemCostNumber=new BigDecimal(0.00);
         }
         itemCostNumber = itemCostNumber.setScale(2,BigDecimal.ROUND_HALF_UP);
         itemCost = itemCostNumber.toString(); 
         
         if(preTaxPreFreTotalNumber==null) {
           msg("preTaxPreFreTotalNumber==null");
           preTaxPreFreTotalNumber=new BigDecimal(0.00);
         }


       //if(itemCostNumber.doubleValue()==0) {}
         if(preTaxPreFreTotalNumber.doubleValue()==0) {
           logger.warn("LineItem ["+lineNumber+"] on contract[" +(p+1)+"] id["+ contractNbr2+"] zero["+preTaxPreFreTotalNumber.toString()+"]");
         }else {
         //logger.info("LineItem ["+lineNumber+"] on contract[" +(p+1)+"] id["+ contractNbr2+"] amt["+itemCost+"]");
         }
         
         preTaxPreFreTotal = rset2.getString("PRE_TAX_PRE_FRE_TOTAL");
         costCenter = rset2.getString("COST_CENTER");
         productId = rset2.getString("PRODUCT_ID");
         account = rset2.getString("GL_ACCOUNT");

         activityId = rset2.getString("ACTIVITY_ID");
         resourceType = rset2.getString("RESOURCE_TYPE");

       //activityId = "act002";
       //resourceType = "res056";

         String formattedDueDate = "";
         
         if(dueDate != null) {
           formattedDueDate = new SimpleDateFormat("dd-MMM-yyyy").format(dueDate);
         }else {
           msg("WARN: dueDate null");
         }
         
         itsb.setRowId(rowId);
         itsb.setHeaderCtrlNumber(itemHdrCtrlNumber);
         itsb.setContractNumber(contractNumber);
         
         quantity = moranvilleQuantity;
         itemCost = moranvilleItemCost;
         
       //if(itemCostNumber.doubleValue() != 0) {}
         if(preTaxPreFreTotalNumber.doubleValue() != 0) {
        // outPut the record
           lineItemsExtracted++;
           nonZer0Dollarlines++;
           if(hsb.isWrittenOut()) {
          // do nothing. header has already been written
           }else {
             msg(EncoreToFCOEExtract.createHdrStgRecord(hdrBusinessUnit, contractNumber+contractNumSffx, docNumber2, currency, vendorId, contractNumber+contractNumSffx, vendorId, hdrBusinessUnit, currency));
             writeLineToFile(EncoreToFCOEExtract.createHdrStgRecord(hdrBusinessUnit, contractNumber+contractNumSffx, docNumber2, currency, vendorId, contractNumber+contractNumSffx, vendorId, hdrBusinessUnit, currency)+CRLF);
             hsb.setWrittenOut(true);
             headersArrLst.add(hsb);
             headersExtracted++;
           }
           
           lineItemsArrLst.add(itsb); // rows to be updated later 
           msg(EncoreToFCOEExtract.createDistStgRecord(hdrBusinessUnit, contractNumber+contractNumSffx, lineNumber, quantity, itemCost, currency, merchandiseAmountNumber.toString(), account, costCenter, productId, itmBusinessUnit, pcBusinessUnit, projectId, activityId, resourceType));
           msg(EncoreToFCOEExtract.createItmStgRecord(hdrBusinessUnit, contractNumber+contractNumSffx, lineNumber, formattedDueDate, vendorId, "ENCORE", "EA", quantity, itemCost, preTaxPreFreTotal, "DES", contractNumber+contractNumSffx, merchandiseAmountNumber.toString(), currency, itemCost, "EA"));
           writeLineToFile(EncoreToFCOEExtract.createDistStgRecord(hdrBusinessUnit, contractNumber+contractNumSffx, lineNumber, quantity, itemCost, currency, merchandiseAmountNumber.toString(), account, costCenter, productId, itmBusinessUnit, pcBusinessUnit, projectId, activityId, resourceType)+CRLF);
           writeLineToFile(EncoreToFCOEExtract.createItmStgRecord(hdrBusinessUnit, contractNumber+contractNumSffx, lineNumber, formattedDueDate, vendorId, "ENCORE", "EA", quantity, itemCost, preTaxPreFreTotal, "DES", contractNumber+contractNumSffx, merchandiseAmountNumber.toString(), currency, itemCost, "EA")+CRLF);
         }
       } // end of while rset2.next inner loop
       logger.info("contract["+contractNbr2+"] lineItemsOnThisContract["+ lineItemsOnThisContract+"] nonZer0Dollarlines["+nonZer0Dollarlines+"]");

     } // end of 1st contractsArrLst outer for-loop

     
    }catch(Exception e){
   // big problem if we wind up here
      jReturnValue++;
      emsg = "extracting data. caught :\n" + getStack(e);
      devGroupMsgs += "\n" + emsg;
      logger.error(emsg);
      emsg="";
    }

    if(lineItemsExtracted>0) {
    //logger.info("Performing sftp send lineItemsExtracted["+lineItemsExtracted+"]");
      logger.warn("!Sftp Send to be performed by external script!  lineItemsExtracted["+lineItemsExtracted+"]");    	
    //sftpFileSend = send();
      sftpFileSend = true;
    }else {
      logger.info("No sftp send: lineItemsExtracted["+lineItemsExtracted+"] ");
      logger.info("No database records to update: lineItemsExtracted["+lineItemsExtracted+"] ");
    }

    msg("");
    HdrStgBean hstb2=null;
    ItmStgBean istb2=null;
    
    if(sftpFileSend) {
    //logger.info("sftp put operation of "+props.getProperty("localFile") + " OK");
      logger.info("update pushed records in the dataBase");
      for(int ax=0; ax<headersArrLst.size(); ax++) {
        hstb2 = null;
        
        hstb2 = headersArrLst.get(ax);
        try {
        	logger.info("Header contractnumber: "+hstb2.getContractNumber()+" hdr: "+hstb2.getHeaderCtrlNumber());
          pstmt3.setString(1,hstb2.getContractNumber());
          pstmt3.setObject(2,hstb2.getHeaderCtrlNumber());
          int rupd1=0;
          pstmt3.executeUpdate();  
          recordsUpdated += rupd1;
          msg("hdr: ["+hstb2.getRowId()+"] ["+hstb2.getHeaderCtrlNumber()+"] rupd1["+rupd1+"] contract["+hstb2.getContractNumber()+"] doc#["+hstb2.getDocNumber()+"]");
        //dbconn.rollback();
          dbconn.commit();
        }catch(Exception e1) {
        //jReturnValue++;
          emsg="problem updating hdr ["+hstb2.getRowId()+"] ["+hstb2.getHeaderCtrlNumber()+"] contract["+hstb2.getContractNumber()+"]";
          devGroupMsgs += "\n" + emsg ; 
          msg("WARN: " +emsg);
          logger.warn(emsg);           
          emsg="";
        }
      //msg("hdr: ["+hstb2.getRowId()+"] ["+hstb2.getHeaderCtrlNumber()+"]");
      }   
      for(int bx=0; bx<lineItemsArrLst.size(); bx++) {
        istb2=null;
        
        istb2 = lineItemsArrLst.get(bx);
        try {
          pstmt4.setString(1,istb2.getContractNumber());
          pstmt4.setObject(2,istb2.getHeaderCtrlNumber());
          int rupd2=0;
          pstmt4.executeUpdate();
          recordsUpdated += rupd2;
          msg("itm: ["+istb2.getRowId()+"] ["+istb2.getHeaderCtrlNumber()+"] rupd2["+rupd2+"] contract["+istb2.getContractNumber()+"]");
        //dbconn.rollback();
          dbconn.commit();
        }catch(Exception e1) {
            //jReturnValue++;
            emsg="problem updating item ["+istb2.getRowId()+"] ["+istb2.getHeaderCtrlNumber()+"] contract["+istb2.getContractNumber()+"]";
            devGroupMsgs += "\n" + emsg ; 
            msg("WARN: " +emsg);
            logger.warn(emsg);           
            emsg="";

        }
      //msg("itm: ["+istb2.getRowId()+"] ["+istb2.getHeaderCtrlNumber()+"]");
      }   
    
    }else {
      if(lineItemsExtracted>0) {
     // send was attempted something went wrong 
        logger.error("sftp put operation of "+props.getProperty("remoteFile") + " failed");
      }else {
     // send was not attempted since no line items qualified
      //logger.info("no sftp put operation lineItemsExtracted["+lineItemsExtracted+"]");
        logger.info("no LineItems Extracted lineItemsExtracted["+lineItemsExtracted+"]");
      }
    }



    logger.info("contractNumbersExamined="+contractNumbersExamined);
    logger.info("headersExtracted=" + headersExtracted);
    logger.info("lineItemsExtracted=" + lineItemsExtracted);
    logger.info("recordsUpdated=" + recordsUpdated);
    
    devGroupMsgs += "\n" + "contractNumbersExamined="+contractNumbersExamined;
    devGroupMsgs += "\n" + "headersExtracted=" + headersExtracted;
    devGroupMsgs += "\n" + "lineItemsExtracted=" + lineItemsExtracted;
    devGroupMsgs += "\n" + "recordsUpdated=" + recordsUpdated;


    emsg += " - end";
    msg("mailing dev group");
    mailDevGroup(devGroupMsgs);
    msg("dev group mailed");

  } // end of runMe method


  private static void init() throws Exception {
   env = System.getProperty("tririgaEnv");

   log4jProps = getLogProperties();
// Set up configuration for logging.
// BasicConfigurator.configure();
   PropertyConfigurator.configure(log4jProps); // it works even without this line


// tririgaBatch.dev.cfg
  //msg(new java.util.Date() + ": loading properties");
    logger.info("start: loading properties. env=[" + env + "]");


    props = new Properties();

  //String configFile = cfgFileName + ".L0." + cfgFileExt;
  //String configFile = cfgFileName + "." + env + "." + cfgFileExt;
    String configFile = cfgFileName + "." + cfgFileExt;


  // from Hrt
    InputStream cfgFileStrm = null;
    cfgFileStrm = Thread.currentThread().getContextClassLoader().getResourceAsStream(configFile);

  //msg(configFile + " found");
    if(cfgFileStrm != null) {
      logger.debug(configFile + " found.");
    }else {
      logger.error(configFile + " NOT found");
    }
     
    String contents = streamToString(cfgFileStrm).toString();
    props.load(new DataInputStream(new ByteArrayInputStream(contents.getBytes())));

    initProps = true;


    InputStream fileStrm = null;
    fileStrm = Thread.currentThread().getContextClassLoader().getResourceAsStream(keyFileName);
    if(fileStrm != null) {
      logger.debug(keyFileName + " found");
    }else {
      logger.error(keyFileName + " NOT found");
    }

      
  //msg(new java.util.Date() + ": initializing encryption");
    logger.info("initializing encryption");

  //aes = new AesEncr(keyFileName);
  //aes.setKey(skey);
  //aes.instantiateCipher();

    emailHost       = props.getProperty("emailHost");
    excEmailTo      = props.getProperty("mailTo");
    excEmailFrom    = props.getProperty("mailFrom");
    emailSubjectExc = taskName + " " + env;

    String dbUrl = PropertyUtility.getProperty("ENCORE_JDBC_URL");
    String dbUser = PropertyUtility.getProperty("ENCORE_INTCLIENT_JDBC_USER");
  //String dbPasswdHash = PropertyUtility.getProperty("ENCORE_INTCLIENT_JDBC_PASSWORD");
    String dbPasswd = "";

  //dbPasswd = aes.deCrypt(dbPasswdHash); // decrypt
  //dbPasswd = PropertyUtility.getEncryptedProperty("ENCORE_INTCLIENT_JDBC_PASSWORD");
    dbPasswd="VCi2ClsSzf2BhoWKHochb66U7";
    msg("604 dbPasswd "+dbPasswd);
 // connect to the db

 // Load the Oracle JDBC driver
  //msg(new java.util.Date() + ": register jdbc driver");
    logger.info("register jdbc driver");
    DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());

  //msg(new java.util.Date() + ": get db connection");
    logger.info("get db connection");
    dbconn = DriverManager.getConnection (dbUrl,dbUser,dbPasswd);
    dbconn.setAutoCommit(false);
    initDbConn = true;
    dbPasswd =  null; // nuke it

    sftpSiteHost = props.getProperty("SftpSiteHost");
    sftpSitePort = props.getProperty("SftpSitePort");
    sftpSiteUser = props.getProperty("SftpSiteUser");

    String sftpSitePwdParam = props.getProperty("SftpSitePwd");
  //sftpSitePwd = aes.deCrypt(sftpSitePwdParam); // decrypt
    
    
    
  } // end of init method

  private static Properties getLogProperties() throws Exception {

    Properties p = new Properties();
    Thread myThread = Thread.currentThread();
    ClassLoader loader = myThread.getContextClassLoader();
    InputStream istrm = loader.getResourceAsStream(log4jCfgFileName);

    String contents = streamToString(istrm).toString();
    p.load(new DataInputStream(new ByteArrayInputStream(contents.getBytes())));
    return p;
  } 


// from Hrt:
  /** ******************************************
   * <b>Purpose:</b> Converts the "lines" of data on a stream into a \n delimited string
   * so they can be passed around the rest of the program and manipulated.
   * Replaces the functionality from HrFileHandler.getFile(filename) which cannot read JAR's
   *
   * @throws Exception all general, non-trapped errors raised, including stream errors.
   * @param  <i>instream</i> input stream containing name/value pair parameter values.
   * @return StringBuffer = contents of specified file formatted as a \n delimited String.
   ******************************************** **/
 private static StringBuffer streamToString(InputStream instream) throws Exception {
     BufferedReader br = null;
     try {
        br = new BufferedReader(new InputStreamReader(instream));
        StringBuffer sbuf = new StringBuffer();
        String sLine = null;
        while ((sLine = br.readLine ()) != null) {
          sbuf.append(sLine + '\n');
        }
        return sbuf;
    // interesting ... no catch block
     }finally {
        if(br !=null) {br.close();}
     }
  }

  private static String getStack(Throwable t)  {
    String sout = "";
    StringWriter sw = new StringWriter();
    PrintWriter pw = new PrintWriter(sw);
    t.printStackTrace(pw);
    sout =  sw.toString();
    pw.close();
    try {
      sw.close();
    } catch(Exception exc) {}
      return sout;
  }


  private static void waitMills(int mills) {
    try {Thread.sleep(mills);}catch(Exception e){}
  }

  private static void msg(Object o) {
    System.out.println("" + o);
  }

  private static String nullCheck(String str){
    return str==null ? "" : str;
  }

  static BigDecimal nullCheck(BigDecimal bd) {
    return bd==null ? new BigDecimal(0) : bd;
  }

  static String getField(String in, String dl, int inum) {
    String sout = "";
    Vector v = GTokenizer1.tokenize1(in,dl);
    if(inum > 0 && inum <= v.size()) {
      sout = (String) v.elementAt(inum -1);
      sout = sout.trim();
    }
    return sout;
  }


  private static void mailDevGroup(String letter) {
    try {
      TrUtil.sendEmail(letter,emailSubjectExc,excEmailTo,null,emailHost,excEmailFrom);
    }catch(Exception e) {
      msg("sending email. caught:" + e + "\n" + getStack(e));
    }
  }

  private static boolean send() {
    boolean bval = false;

 // get params
    String localFile = props.getProperty("remoteFile");
    logger.info("Sending data to " + sftpSiteHost + ":" + sftpSitePort);

    String remotePath = "";
  //remotePath = getRemoteFilePath(props,sendTime);
    remotePath = props.getProperty("remoteFile");

 // ScpClient scp = null;
    SftpClient sftp = null;
    SSHHelper helper = null;
 // String keyFingerprint = "1030: 80 30 34 b3 82 98 7 12 f5 51 8e 9c f8 dd 62 b5"; // chsux09.corp.fedex.com

    int iPort = 22; // usual sftp default
    try {
      iPort = Integer.parseInt(sftpSitePort);
    }catch(Exception e) {
      logger.error("getting sftp port from cfg: " + e,e);
    }

   helper = new SSHHelper();

// Set connection attribute(s)
   helper.setVerbose(false); // Optional... for debugging
   helper.setKnownHostName(sftpSiteHost);
// helper.setKnownKeyFingerprint(keyFingerprint); // Optional... for validation

// Establish the connection
   logger.info("Connecting to " + sftpSiteHost + ":" + iPort);
   try {
  // helper.connectToServer();
     helper.connectToServer(iPort);
  // Compare hostname/key from server to known values. Optional step.
  // helper.validateServer();

   }catch(Exception e) {
     logger.error("connecting to " + sftpSiteHost + ":" + iPort + "  caught:" + e,e);
   }

   try {
  // scp = helper.getSCPClient(userName,userPasswd);  // Get a SCP client
     sftp = helper.getSFTPClient(sftpSiteUser,sftpSitePwd);  // Get a SCP client
   }catch(Exception e) {
     logger.error("getting sftp client for " + sftpSiteHost + ":" + iPort + "  caught:" + e,e);
   }

   logger.info("performing sftp.put  file -> " + remotePath);
   try {
  // scp.put(localFile,remotePath,false);  // do the file copy
     sftp.put(localFile,remotePath);  // do the file copy
     bval = true;
   }catch(Exception e) {
     logger.error("sftp put operation for " + sftpSiteHost + ":" + iPort + "  " + remotePath + "   caught:" + e,e);
   }

   // nuke
   sftpSiteUser = null; 
   sftpSitePwd = null;

   logger.info("sftp disconnecting");

   try {
     helper.disconnect(); // close the connection
   }catch(Exception e) {
     logger.error("disconnecting from " + sftpSiteHost + ". caught:" + e,e);
   }

   return bval;
  } // end of send() method

  static BigDecimal sumMyBigDecimals(BigDecimal[] bds) {  
    BigDecimal bd1 = new BigDecimal(0);
    for(int x=0; x<bds.length; x++) {
      bd1=bd1.add(bds[x]);
    }
    return bd1;
  }
  
  private static void cleanUp() {
  //msg(new java.util.Date() + ": closing connections and streams");
    logger.info("closing connections and streams");
 // close the database connection
    try {
      TrUtil.closeDbConn(dbconn);
    }catch(Exception e) {
    //msg("closing db connection: caught:" + e + "\n" + getStack(e));
      logger.error("closing db connection: caught:" + e + "\n" + getStack(e));
    }

 // nuke
    aes =  null;

  } // end cleanUp

  static void writeLineToFile(String in) throws Exception {
    File outf2 = new File(props.getProperty("remoteFile"));
    FileWriter fr2 = new FileWriter(outf2,true);
    BufferedWriter buf = new BufferedWriter(fr2);   
    buf.write(in);
    buf.close();
  }


}
