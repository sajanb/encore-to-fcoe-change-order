/**
 
 08/2013

*/
package com.fedex.encore.fcoe;

import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.Vector;

import java.io.*;
import java.math.BigDecimal;

import java.text.SimpleDateFormat;
import java.text.DateFormat;

import javax.crypto.SecretKey;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

// Import log4j classes.
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.fedex.encore.util.AesEncr;
import com.fedex.encore.util.GTokenizer1;
import com.fedex.encore.util.PropertyUtility;
//import com.fedex.encore.util.RowHelper;
import com.fedex.encore.util.SSHHelper;
import com.fedex.encore.util.TrUtil;
import com.sshtools.j2ssh.SftpClient;
import com.fedex.encore.fcoe.*;


public class FcoeCOOutbound {

  private static String log4jCfgFileName = "fcoeOutBoundLog4j.properties";

  static Logger logger = Logger.getLogger(FcoeCOOutbound.class);
  private static final String taskName = "FcoeOutbound";
  private static String env = "";
  private static String action = "";
  static final String FIELDPARAMPREFIX = "Fmt";
  static int recordLength = 0;
  
  private static boolean initProps = false;
  private static boolean initDbConn = false;
  private static boolean sftpFileSend = false;
  private static boolean sftpFileSend1 = false;

  private static Properties props = null;
  private static Properties log4jProps = null;

  private static Connection dbconn = null;
  private static Connection dbconn_fcoe = null;
  private static PreparedStatement pstmt1 = null;
  private static PreparedStatement pstmt2 = null;
  private static PreparedStatement pstmt3 = null;
  private static PreparedStatement pstmt4 = null;
  private static PreparedStatement pstmt5 = null;
  private static PreparedStatement pstmt6 = null;
  private static PreparedStatement pstmt7 = null;
  private static PreparedStatement pstmt8 = null;
  private static PreparedStatement pstmt9 = null;

  private static ResultSet rset1 = null;
  private static ResultSet rset2 = null;
  private static ResultSet rset3 = null;
  private static ResultSet rset4 = null;
  private static ResultSet rset5 = null;
  private static AesEncr aes = null;

  static String cfgFileName = "fcoeOutbound";
  static String cfgFileExt = "cfg";

  static String keyFileName = "g10.k";

  static String emailHost       = "";
  static String excEmailTo      = "";
  static String excEmailFrom    = "";
  static String emailSubjectExc = "";
  static String devGroupMsgs = "";
  static String emsg = "";

  static ArrayList<HdrStgBean> contractsArrLst = null;
  static ArrayList<HdrStgBean> COArrLst = null;
  static ArrayList<HdrStgBean> headersArrLst = new ArrayList();;
  static ArrayList<ItmStgBean> lineItemsArrLst = new ArrayList();
  static ArrayList<HdrStgBean> headersArrLst1 = new ArrayList();;
  static ArrayList<ItmStgBean> lineItemsArrLst1 = new ArrayList();
//static ArrayList<RowHelper> rowsArrLst = null;

  static char[] recordCharsArr = null;
  static char[] crlfarr = {13,10};
  private static final String CRLF = new String(crlfarr);
  
  private static String sftpSiteHost = ""; 
  private static String sftpSitePort = ""; 
  private static String sftpSiteUser = "";
  private static String sftpSitePwd = "";
  static int jReturnValue=0;

  public static void main(String[] args) {
    String st1 = "7";
    String st2 = "7";
    emsg += " test start";
    
    try {
      init();
    }catch(Exception e) {
      emsg = "running init() caught:" + e + "\n" + getStack(e) + "\nExiting"; 
      msg(emsg);
      logger.error(emsg);
   // no use continuing
      cleanUp();
      jReturnValue=1;
      msg(new Date()+": java "+taskName+" exiting with value ["+jReturnValue+"]");
      System.exit(jReturnValue);

    }

    runMe(st1,st2);
    cleanUp();

    msg(new Date()+": java "+taskName+" exiting with value ["+jReturnValue+"]");
    FcoeOutbound.main(null);
    System.exit(jReturnValue);
  }

  private static void runMe(String marg, String marg2) {

    String localHostName = "";
    String cwd = ""; 
    File fcwd = new File(".");
    java.net.InetAddress localHost = null;

    try {
      localHostName = java.net.InetAddress.getLocalHost().getCanonicalHostName();    
      cwd=fcwd.getCanonicalPath(); 
    }catch(Exception uhe) {}

    logger.info("running. host["+localHostName+"] cwd["+cwd+"]");

  //msg(new java.util.Date() + ": " + taskName + " running");
    int headersExtracted = 0;
    int lineItemsExtracted = 0;
    int lineitems = 0;
    int recordsUpdated = 0;
    int contractNumbersExamined = 0;
    int lineItemsOnThisContract = 0;
    int nonZer0Dollarlines = 0;
    contractsArrLst = new ArrayList<HdrStgBean>();
    COArrLst = new ArrayList<HdrStgBean>();
    HdrStgBean hsb=null; 
    HdrStgBean hsb1=null;
    ItmStgBean itsb=null;
    ItmStgBean itsb1=null;
    

    String localFilePath = "";
    localFilePath = props.getProperty("remoteFile1");
    
    String localFilePath1 = "";
    localFilePath1 = props.getProperty("remoteFile2");
    
 // delete for fresh start
    logger.info("deleting output 1 file for fresh start");
    File outputFile = new File(props.getProperty("remoteFile1"));
    if(outputFile.exists()) {outputFile.delete();}
    
    logger.info("deleting output 2 file for fresh start");
    File outputFile1 = new File(props.getProperty("remoteFile2"));
    if(outputFile1.exists()) {outputFile1.delete();}
    
    int rowsForTab = 0;
    String fileRecord = "";
    try {
      pstmt1 = dbconn.prepareStatement(props.getProperty("HdrStgSql3"));
      pstmt2 = dbconn.prepareStatement(props.getProperty("ItemStgSql4"));
      pstmt5 = dbconn_fcoe.prepareStatement(props.getProperty("RemainingBalanceSql"));
      pstmt6 = dbconn.prepareStatement(props.getProperty("updateHdrSql2"));
      pstmt7 = dbconn.prepareStatement(props.getProperty("updateItmSql2"));
      
      

      logger.info("getting initial list of contractNumbers");
   // get the list of contractNumbers
      rset1 = pstmt1.executeQuery();
      
   // BUSINESS_UNIT,CONTRACT_NUMBER,CURRENCY,VENDOR_ID f
      String rowId="";
      BigDecimal headerHdrCtrlNumber=null;
      BigDecimal itemHdrCtrlNumber=null;
      String hdrBusinessUnit="";
      String itmBusinessUnit="";
      String contractNumber="";
      String contractNumSffx="";

      String docNumber="";
      String currency="";
      String vendorId="";
      String pcBusinessUnit="";
      String projectId="";
      HdrStgBean hdrStg=null;
      HdrStgBean hdrStg1=null;
      
      logger.info("walking rset1 (header records)");
      while(rset1.next()) {
     // clear em
        rowId="";
        headerHdrCtrlNumber=null;
        contractNumber="";
        contractNumSffx="";

        docNumber="";
        hdrBusinessUnit="";
        currency="";
        vendorId="";
        hdrStg=null;
        
        rowId = nullCheck(rset1.getString("ROWID"));
        headerHdrCtrlNumber = rset1.getBigDecimal("HDR_CTRL_NUMBER");
        contractNumber = rset1.getString("CONTRACT_NUMBER");
        contractNumSffx = nullCheck(rset1.getString("CONTRACT_NUM_SUFFIX")); // contract amendment number
      //contractNumber=contractNumber+contractNumSffx;
        
        docNumber = rset1.getString("DOC_NUMBER");
        hdrBusinessUnit = rset1.getString("BUSINESS_UNIT");
        currency = rset1.getString("CURRENCY");
        vendorId = rset1.getString("VENDOR_ID");

        hdrStg = new HdrStgBean();
        hdrStg.setRowId(rowId);
        hdrStg.setHeaderCtrlNumber(headerHdrCtrlNumber);
        hdrStg.setBusinessUnit(hdrBusinessUnit);
        hdrStg.setContractNumber(contractNumber);
        hdrStg.setDocNumber(docNumber);
        hdrStg.setCurrency(currency);
        hdrStg.setVendorId(vendorId);
        
        msg(EncoreToFCOEExtract.createHdrStgRecord(hdrBusinessUnit, contractNumber+contractNumSffx, docNumber, currency, vendorId, contractNumber+contractNumSffx, vendorId, hdrBusinessUnit, currency));
        
        contractNumbersExamined++;
        msg("adding record["+contractNumbersExamined+"] "+rowId+"|"+contractNumber+contractNumSffx+"|"+docNumber);
        contractsArrLst.add(hdrStg);
      }
      
      rset1.close();

      if(true) {
     // msg("DEBUG returning");
     // return;
      }
      
     Hashtable hrow = null; 

  // declare Strings for individual column and file record values
  // ItemStgSql1=select BUSINESS_UNIT,CONTRACT_NUMBER,LINE_NUMBER,DUE_DATE,VENDOR_ID,QUANTITY,ITEM_COST,PRE_TAX_PRE_FRE_TOTAL,CURRENCY,COST_CENTER,PRODUCT_ID from FDX_CONTRACT_ITEMS_OUT where CONTRACT_NUMBER=?
     
     itmBusinessUnit=""; // BUSINESS_UNIT
     contractNumber=""; // CONTRACT_NUMBER
     contractNumSffx=""; // CONTRACT_NUM_SUFFIX contract amendment number

     currency=""; // CURRENCY
     vendorId=""; // VENDOR_ID
   //String rowId="";
     String lineNumber=""; // LINE_NUMBER
     String remainingbal_lineNumer="";//Remaining balance Line number
     String remainingbal_contrnum="";//Remaining balane contract number
     Date dueDate=null; // DUE_DATE
     String quantity=""; // QUANTITY 
     String moranvilleQuantity="1"; // QUANTITY 
     String moranvilleItemCost="0.00"; // ITEM_COST 
     String itemCost=""; // ITEM_COST
     BigDecimal itemCostNumber=null; // ITEM_COST
     String preTaxPreFreTotal=""; // PRE_TAX_PRE_FRE_TOTAL
     BigDecimal preTaxPreFreTotalNumber=null; // PRE_TAX_PRE_FRE_TOTAL
     BigDecimal freightAmountNumber=null;
     BigDecimal reimburseAmountNumber=null;
     BigDecimal merchandiseAmountNumber=null;
     BigDecimal remainingBalance=null;
     BigDecimal[] amounts=null; 
     BigDecimal subValue=null;

     String costCenter=""; // COST_CENTER
     String productId; // PRODUCT_ID

     String activityId; // ACTIVITY_ID - new Jan 2014
     String resourceType; // RESOURCE_TYPE - new Jan 2014

     String account; // ACCOUNT
     String tmpString="";
     lineItemsArrLst = new ArrayList();
     headersArrLst = new ArrayList();
     Boolean hasRemaining_Bal;
     
   // loop through projects found
     logger.info("walking contractsArrLst["+contractsArrLst.size()+"] contracts found");
     for(int p = 0; p < contractsArrLst.size(); p++) {
       hsb=null; // new contract. clear it
       hdrBusinessUnit="";
       
      // set it 
       hsb=contractsArrLst.get(p);
       hsb.setWrittenOut(false);
       String contractNbr2 = hsb.getContractNumber();
       BigDecimal headerHdrCtrlNumberFromHeaderBean = hsb.getHeaderCtrlNumber();
       String docNumber2 = hsb.getDocNumber(); // DOC_NUMBER from hdr
       hdrBusinessUnit = hsb.getBusinessUnit(); // hdr bu
       msg("");
       logger.info("processing contract[" +(p+1)+"] id["+ contractNbr2+"] hdrCtrlId["+hsb.getHeaderCtrlNumber()+"]"); 

       pstmt2.setObject(1,contractNbr2);
       pstmt2.setObject(2,headerHdrCtrlNumberFromHeaderBean);
       
       rset2=pstmt2.executeQuery();
       lineItemsOnThisContract = 0; // reset it for new contract hdr
       nonZer0Dollarlines = 0;

       while(rset2.next()) {
         lineItemsOnThisContract++;
      // clear em
         itsb=null; // ItmStgBean - lineItem bean
         itemHdrCtrlNumber=null; // HDR_CTRL_NUMBER
         itmBusinessUnit=""; // BUSINESS_UNIT
         pcBusinessUnit=""; // PC_BU
         projectId=""; // PROJECT_ID
         contractNumber=""; // CONTRACT_NUMBER
         contractNumSffx=""; // CONTRACT_NUM_SUFFIX contract amendment number

         currency=""; // CURRENCY
         vendorId=""; // VENDOR_ID
         rowId="";
         lineNumber=""; // LINE_NUMBER
         dueDate=null; // DUE_DATE
         quantity=""; // QUANTITY 
         itemCost=""; // ITEM_COST
         itemCostNumber=null; // ITEM_COST
         preTaxPreFreTotal=""; // PRE_TAX_PRE_FRE_TOTAL
         preTaxPreFreTotalNumber=null; // 
         freightAmountNumber=null; // FREIGHT_AMOUNT
         reimburseAmountNumber=null; // REIMBURSE_AMOUNT
         merchandiseAmountNumber=null; // sum PRE_TAX_PRE_FRE_TOTAL+FREIGHT_AMOUNT+REIMBURSE_AMOUNT
         amounts = new BigDecimal[3];
         hasRemaining_Bal = false;

         costCenter=""; // COST_CENTER
         productId=""; // PRODUCT_ID
         account=""; // ACCOUNT

         itsb= new ItmStgBean();
         rowId=rset2.getString("ROWID");
         
        
         itemHdrCtrlNumber = rset2.getBigDecimal("HDR_CTRL_NUMBER");
         itmBusinessUnit = rset2.getString("BUSINESS_UNIT"); // BUSINESS_UNIT
         pcBusinessUnit = rset2.getString("PC_BU"); // PC_BU
         projectId = rset2.getString("PROJECT_ID"); // PROJECT_ID
         contractNumber = rset2.getString("CONTRACT_NUMBER");
         contractNumSffx = nullCheck(rset2.getString("CONTRACT_NUM_SUFFIX"));
       //contractNumber=contractNumber+contractNumSffx;

         currency = rset2.getString("CURRENCY");
         vendorId = rset2.getString("VENDOR_ID");

         lineNumber = rset2.getString("LINE_NUMBER");
         dueDate = (Date) rset2.getObject("DUE_DATE");
         quantity = rset2.getString("QUANTITY");
         itemCost = rset2.getString("ITEM_COST");
         itemCostNumber = rset2.getBigDecimal("ITEM_COST");
         preTaxPreFreTotalNumber = nullCheck(rset2.getBigDecimal("PRE_TAX_PRE_FRE_TOTAL"));
         freightAmountNumber     = nullCheck(rset2.getBigDecimal("FREIGHT_AMOUNT"));
         reimburseAmountNumber   = nullCheck(rset2.getBigDecimal("REIMBURSE_AMOUNT"));
         if(contractNumSffx.length()<3)
         {
        	 logger.info("Error with contract number: "+contractNumber+" with hdrcntrlno: "+headerHdrCtrlNumberFromHeaderBean+" and suffix: "+contractNumSffx);
        	 //rset2.TYPE_SCROLL_INSENSITIVE();
        	 if(rset2.next());
        	 	break;
         }
         String suffix = contractNumSffx.substring(0, 3);
         String pass_contrnumber = contractNumber+suffix;
         logger.info("Passing value "+pass_contrnumber+","+lineNumber+","+preTaxPreFreTotalNumber);
         pstmt5.setObject(1, contractNumber+suffix);
         logger.info("object set");
         rset3 = pstmt5.executeQuery();

         while(rset3.next())
         {
        	 hasRemaining_Bal = true;
        	 remainingbal_lineNumer="";
        	 remainingbal_contrnum="";
        	 remainingBalance=null;
        	 remainingbal_lineNumer=rset3.getString("LINE_NBR");
        	 remainingbal_contrnum=rset3.getString("CONTRACT_NUMBER");
        	 logger.info("Contr number in fcoe "+rset3.getString("CONTRACT_NUMBER"));
        	 remainingBalance=nullCheck(rset3.getBigDecimal("REMAINING_BALANCE"));
        	 logger.info("'Remaining Balance "+rset3.getBigDecimal("REMAINING_BALANCE"));
        	 
        	 if((remainingbal_contrnum.equals(pass_contrnumber))&&(remainingbal_lineNumer.equals(lineNumber)))
        	 {
        		 subValue = preTaxPreFreTotalNumber.subtract(remainingBalance);
        		 logger.info("sub value "+subValue);
        		 
        		 if(subValue.longValue()>=0)
        		 {
        			 preTaxPreFreTotalNumber =  subValue;
        			 logger.info("Greater than zero "+subValue);
        			 pstmt7.setObject(1, preTaxPreFreTotalNumber);
        			 pstmt7.setObject(2, contractNumber);
        			 pstmt7.setObject(3, lineNumber);
        			 pstmt7.setObject(4,itemHdrCtrlNumber);
        			 pstmt6.setObject(1,contractNumber);
        			 pstmt6.setObject(2,itemHdrCtrlNumber);
        			 pstmt6.executeUpdate();
        			 pstmt7.executeUpdate();
        			 dbconn.commit();
        		 }
        		 else
        		 {
        			 preTaxPreFreTotalNumber= new BigDecimal(0);
        			 logger.info("Lesser than zero "+preTaxPreFreTotalNumber);
        			 mailDevGroup("Amount in Contract Number: "+contractNumber+" in line number :"+lineNumber+" is negative");
        			 pstmt7.setObject(1, preTaxPreFreTotalNumber);
        			 pstmt7.setObject(2, contractNumber);
        			 pstmt7.setObject(3, lineNumber);
        			 pstmt7.setObject(4,itemHdrCtrlNumber);
        			 pstmt6.setObject(1,contractNumber);
        			 pstmt6.setObject(2,itemHdrCtrlNumber);
        			 pstmt6.executeUpdate();
        			 pstmt7.executeUpdate();
        			 dbconn.commit();
        		 }
        	 }
         }
         
         if(hasRemaining_Bal==false)
         {
        	 logger.info("No remaining balance "+preTaxPreFreTotalNumber);
        	 pstmt7.setObject(1, preTaxPreFreTotalNumber);
			 pstmt7.setObject(2, contractNumber);
			 pstmt7.setObject(3, lineNumber);
			 pstmt7.setObject(4,itemHdrCtrlNumber);
			 pstmt6.setObject(1,contractNumber);
			 pstmt6.setObject(2,itemHdrCtrlNumber);
			 pstmt6.executeUpdate();
			 pstmt7.executeUpdate();
			 dbconn.commit();;
         }
         amounts[0]=preTaxPreFreTotalNumber;
         amounts[1]=freightAmountNumber;
         amounts[2]=reimburseAmountNumber;
         
         merchandiseAmountNumber = sumMyBigDecimals(amounts);
         merchandiseAmountNumber = merchandiseAmountNumber.setScale(2,BigDecimal.ROUND_HALF_UP);
         
         if(itemCostNumber==null) {
           msg("itemCostNumber==null");
           itemCostNumber=new BigDecimal(0.00);
         }
         itemCostNumber = itemCostNumber.setScale(2,BigDecimal.ROUND_HALF_UP);
         itemCost = itemCostNumber.toString(); 
         
         if(preTaxPreFreTotalNumber==null) {
           msg("preTaxPreFreTotalNumber==null");
           preTaxPreFreTotalNumber=new BigDecimal(0.00);
         }


         if(preTaxPreFreTotalNumber.doubleValue()==0) {
           logger.warn("LineItem ["+lineNumber+"] on contract[" +(p+1)+"] id["+ contractNbr2+"] zero["+preTaxPreFreTotalNumber.toString()+"]");
         }
         
         costCenter = rset2.getString("COST_CENTER");
         productId = rset2.getString("PRODUCT_ID");
         account = rset2.getString("GL_ACCOUNT");
         activityId = rset2.getString("ACTIVITY_ID");
         resourceType = rset2.getString("RESOURCE_TYPE");

         String formattedDueDate = "";
         
         if(dueDate != null) {
           formattedDueDate = new SimpleDateFormat("dd-MMM-yyyy").format(dueDate);
         }else {
           msg("WARN: dueDate null");
         }
         
         itsb.setRowId(rowId);
         itsb.setHeaderCtrlNumber(itemHdrCtrlNumber);
         itsb.setContractNumber(contractNumber);
         
         quantity = moranvilleQuantity;
         itemCost = moranvilleItemCost;
         lineItemsExtracted++;
         nonZer0Dollarlines++;
       } // end of while rset2.next inner loop
       logger.info("contract["+contractNbr2+"] lineItemsOnThisContract["+ lineItemsOnThisContract+"] nonZer0Dollarlines["+nonZer0Dollarlines+"]");

     } // end of 1st contractsArrLst outer for-loop

    }catch(Exception e){
    	   // big problem if we wind up here
    	      jReturnValue++;
    	      emsg = "extracting data. caught :\n" + getStack(e);
    	      devGroupMsgs += "\n" + emsg;
    	      logger.error(emsg);
    	      emsg="";
    	    }

    logger.info("Into CO");
	try {
		logger.info("Running CO");
		pstmt8 = dbconn.prepareStatement(props.getProperty("HdrStgSql2"));
		pstmt9 = dbconn.prepareStatement(props.getProperty("ItemStgSql3"));
		pstmt3 = dbconn.prepareStatement(props.getProperty("HdrStgUpdateSql"));
	    pstmt4 = dbconn.prepareStatement(props.getProperty("ItemStgUpdateSql"));
		rset4 = pstmt8.executeQuery();
		
		logger.info("Executed Pstmt8");
	    String contractNumber="";
		String contractNumSffx="";
		String currency="";
		String hdrBusinessUnit="";
		String rowId = "";
		String hdrCreatedDate = "";
		String itemCreatedDate = "";
	 	BigDecimal headerHdrCtrlNumber= null;
	 	BigDecimal tririga_recordIdHdr = null;
	 	BigDecimal tririga_recordIdItem = null;		
		//HdrStgBean hdrStg1 = new HdrStgBean();
		
		BigDecimal preTaxPreFreTotalNumber = null; // 
		BigDecimal freightAmountNumber = null; // FREIGHT_AMOUNT
		BigDecimal reimburseAmountNumber = null; // REIMBURSE_AMOUNT
		BigDecimal merchandiseAmountNumber = null;
		BigDecimal itemCostNumber = null;
		String projectId="";
		String glAccount="";
		String costCenter="";		
		String itemCost="";
		String pcBU = " ";
		String activityId = "";
		String resourceType ="";
		logger.info("rset4");
		logger.info("pstm8 "+props.getProperty("HdrStgSql2"));
	
		while(rset4.next())
		 {
			 logger.info("rset4");
			 contractNumber="";
		     contractNumSffx="";
		     currency=""; 
		     hdrBusinessUnit="";
		     hdrCreatedDate = "";
		     tririga_recordIdHdr = null;
		     headerHdrCtrlNumber=null;
		     headerHdrCtrlNumber = rset4.getBigDecimal("HDR_CTRL_NUMBER");
		     contractNumber = rset4.getString("CONTRACT_NUMBER");
		     contractNumSffx = nullCheck(rset4.getString("CONTRACT_NUM_SUFFIX")); // contract amendment number
		     tririga_recordIdHdr = rset4.getBigDecimal("TRIRIGA_RECORD_ID");
		     hdrBusinessUnit = rset4.getString("BUSINESS_UNIT");
		     currency = rset4.getString("CURRENCY");
		     hdrCreatedDate = rset4.getString("HDR_CREATED_DATE");
		     logger.info("Created Date: "+hdrCreatedDate);
		     
		     HdrStgBean hdrStg1 = new HdrStgBean();
		     hdrStg1.setHeaderCtrlNumber(headerHdrCtrlNumber);
		     hdrStg1.setBusinessUnit(hdrBusinessUnit);
		     hdrStg1.setContractNumber(contractNumber);
		     hdrStg1.setCurrency(currency);
		     hdrStg1.setCreatedDate(hdrCreatedDate);
		     hdrStg1.sethdrTririgaRecID(tririga_recordIdHdr);
		     
		     logger.info("get hdr created Date: "+hdrStg1.getCreatedDate());
		     logger.info("get hdr cntr: "+hdrStg1.getContractNumber());
		     logger.info("get hdr hdrcntr: "+hdrStg1.getHeaderCtrlNumber());
		     logger.info("get hdr bu: "+hdrStg1.getBusinessUnit());
		     logger.info("get tririga_rec_id: "+hdrStg1.gethdrTririgaRecID());
		     logger.info("hdrstg1: contract["+contractNumber+"]  headerHdrCtrlNumber["+headerHdrCtrlNumber+"");
		     COArrLst.add(hdrStg1);
		     
		 }
	
		logger.info("coarrlst "+COArrLst.size());
     rset4.close();
     logger.info("IN rset5");
     String contrnum ="";
     String itmBusinessUnit="";
     BigDecimal hdrctrl=null;
     for(int p = 0; p < COArrLst.size(); p++) {
    contrnum = "";
    hdrctrl = null;
    hsb1=null;
     hsb1 = COArrLst.get(p);
     hsb1.setWrittenOut(false);
     logger.info("rset5 contr "+hsb1.getContractNumber());
     logger.info("rset5 hdr "+hsb1.getHeaderCtrlNumber());
     contrnum = hsb1.getContractNumber();
     hdrctrl = hsb1.getHeaderCtrlNumber();
     pstmt9.setString(1,contrnum);
     pstmt9.setObject(2,hdrctrl);
     rset5=pstmt9.executeQuery();
     logger.info("exec rset5");
     while(rset5.next())
     {
    	 itsb1=null;
    	 lineitems++;

         contractNumber=""; // CONTRACT_NUMBER
         contractNumSffx=""; // CONTRACT_NUM_SUFFIX contract amendment number
         projectId = "";
 		 glAccount="";
 		 costCenter="";
 		 pcBU = "";
         activityId = "";
         itemCreatedDate="";
         resourceType ="";
         itmBusinessUnit ="";
         tririga_recordIdItem=null;
         currency=""; // CURRENCY
         //vendorId=""; // VENDOR_ID
         rowId="";
         String lineNumber = ""; // LINE_NUMBER
         Object dueDate = null; // DUE_DATE
         String quantity = ""; // QUANTITY 
         String preTaxPreFreTotal = ""; // PRE_TAX_PRE_FRE_TOTAL
         preTaxPreFreTotalNumber = null; // 
         freightAmountNumber = null; // FREIGHT_AMOUNT
         reimburseAmountNumber = null; // REIMBURSE_AMOUNT
         merchandiseAmountNumber = null; // sum PRE_TAX_PRE_FRE_TOTAL+FREIGHT_AMOUNT+REIMBURSE_AMOUNT
         BigDecimal[] amounts = new BigDecimal[3];

         logger.info("row id "+rset5.getString("ROWID"));
         itsb1= new ItmStgBean();
         rowId = rset5.getString("ROWID");
 
 		 glAccount=rset5.getString("GL_ACCOUNT");
 		 costCenter=rset5.getString("COST_CENTER");
         BigDecimal itemHdrCtrlNumber = rset5.getBigDecimal("HDR_CTRL_NUMBER");
         //itmBusinessUnit = rset5.getString("BUSINESS_UNIT"); // BUSINESS_UNIT
         itmBusinessUnit = hsb1.getBusinessUnit();
         pcBU = rset5.getString("PC_BU"); // PC_BU
         activityId = rset5.getString("ACTIVITY_ID");
         resourceType = rset5.getString("RESOURCE_TYPE");
         projectId = rset5.getString("PROJECT_ID"); // PROJECT_ID
         contractNumber = rset5.getString("CONTRACT_NUMBER");
         contractNumSffx = nullCheck(rset5.getString("CONTRACT_NUM_SUFFIX"));
         currency = rset5.getString("CURRENCY");
         itemCreatedDate = hsb1.getCreatedDate();
         logger.info("hsb1.getCreatedDate() "+hsb1.getCreatedDate());
         tririga_recordIdItem = hsb1.gethdrTririgaRecID();
         logger.info("hsb1.gethdrTririgaRecID() "+hsb1.gethdrTririgaRecID());
         lineNumber = rset5.getString("LINE_NUMBER");
         dueDate = (Date) rset5.getObject("DUE_DATE");
         logger.info("Due date: "+dueDate);
         quantity = rset5.getString("QUANTITY");
         preTaxPreFreTotalNumber = nullCheck(rset5.getBigDecimal("PRE_TAX_PRE_FRE_TOTAL"));
         freightAmountNumber     = nullCheck(rset5.getBigDecimal("FREIGHT_AMOUNT"));
         reimburseAmountNumber   = nullCheck(rset5.getBigDecimal("REIMBURSE_AMOUNT"));
         amounts[0]=preTaxPreFreTotalNumber;
         amounts[1]=freightAmountNumber;
         amounts[2]=reimburseAmountNumber;
         
         merchandiseAmountNumber = sumMyBigDecimals(amounts);
         merchandiseAmountNumber = merchandiseAmountNumber.setScale(2,BigDecimal.ROUND_HALF_UP);
         
        
		if(itemCostNumber==null) {
           msg("itemCostNumber==null");
           itemCostNumber=new BigDecimal(0.00);
         }
         itemCostNumber = itemCostNumber.setScale(2,BigDecimal.ROUND_HALF_UP);
         itemCost = itemCostNumber.toString(); 
         
         if(preTaxPreFreTotalNumber==null) {
           msg("preTaxPreFreTotalNumber==null");
           preTaxPreFreTotalNumber=new BigDecimal(0.00);
         }
         
         String formattedDueDate = "";
         
         if(dueDate != null) {
           formattedDueDate = new SimpleDateFormat("dd-MMM-yyyy").format(dueDate);
         }else {
        	 formattedDueDate = " ";
           msg("WARN: dueDate null");
         }
         itsb1.setRowId(rowId);
         itsb1.setHeaderCtrlNumber(itemHdrCtrlNumber);
         itsb1.setContractNumber(contractNumber);
         logger.info("stg1: contract["+contractNumber+"]  headerHdrCtrlNumber["+headerHdrCtrlNumber+"");
         logger.info("Going to print");
         if(hsb1.isWrittenOut()) {
             // do nothing. header has already been written
              }else {
         logger.info(EncoreToFCOECOExtract.line_000(hsb1.getBusinessUnit(), contractNumber+contractNumSffx, contractNumber+contractNumSffx, currency, hsb1.getCreatedDate(),hsb1.gethdrTririgaRecID().toString()));
         writeCOToFile(EncoreToFCOECOExtract.line_000(hsb1.getBusinessUnit(), contractNumber+contractNumSffx, contractNumber+contractNumSffx, currency, hsb1.getCreatedDate(),hsb1.gethdrTririgaRecID().toString()));
         hsb1.setWrittenOut(true); 
         headersArrLst1.add(hsb1);
         logger.info("hdrarray size "+headersArrLst.size());
              }
         lineItemsArrLst1.add(itsb1);
         logger.info(EncoreToFCOECOExtract.line_001(itmBusinessUnit, contractNumber+contractNumSffx, lineNumber, itemCreatedDate,tririga_recordIdItem.toString()));
         logger.info(EncoreToFCOECOExtract.line_002(itmBusinessUnit, contractNumber+contractNumSffx, lineNumber, preTaxPreFreTotalNumber.toString(), formattedDueDate, quantity, merchandiseAmountNumber.toString(), currency, itemCreatedDate,tririga_recordIdItem.toString()));
         logger.info(EncoreToFCOECOExtract.line_003(contractNumber+contractNumSffx, itmBusinessUnit, lineNumber, glAccount, costCenter, projectId, pcBU, activityId, resourceType, itemCreatedDate,tririga_recordIdItem.toString()));
         
         writeCOToFile(EncoreToFCOECOExtract.line_001(itmBusinessUnit, contractNumber+contractNumSffx, lineNumber, itemCreatedDate,tririga_recordIdItem.toString()));
         writeCOToFile(EncoreToFCOECOExtract.line_002(itmBusinessUnit, contractNumber+contractNumSffx, lineNumber, preTaxPreFreTotalNumber.toString(), formattedDueDate, quantity, merchandiseAmountNumber.toString(), currency, itemCreatedDate,tririga_recordIdItem.toString()));
         writeCOToFile(EncoreToFCOECOExtract.line_003(contractNumber+contractNumSffx, itmBusinessUnit, lineNumber, glAccount, costCenter, projectId, pcBU, activityId, resourceType, itemCreatedDate,tririga_recordIdItem.toString()));
         //headersArrLst.add(hsb1);
     }
     } 
	}catch (SQLException e) {
		  jReturnValue++;
	      emsg = "extracting data. caught :\n" + getStack(e);
	      devGroupMsgs += "\n" + emsg;
	      logger.error(emsg);
	      emsg="";
 	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
     
     
     logger.info("lineItemsExtracted: "+lineItemsExtracted);
     logger.info("lineitems: "+lineitems);
    

    /*if(lineItemsExtracted>0) {
      logger.warn("!Sftp Send to be performed by external script!  lineItemsExtracted["+lineItemsExtracted+"]");  
      sftpFileSend1 = true;
    }*/
    if(lineitems>0) {
        //logger.info("Performing sftp send lineItemsExtracted["+lineItemsExtracted+"]");
          logger.warn("!Sftp Send to be performed by external script!  lineItems["+lineitems+"]");    	
        //sftpFileSend = send();
          sftpFileSend = true;
        }else {
      logger.info("No sftp send: lineItemsExtracted["+lineItemsExtracted+"] ");
      logger.info("No database records to update: lineItemsExtracted["+lineItemsExtracted+"] ");
    }

    msg("");//yes i can 
    HdrStgBean hstb2=null;
    ItmStgBean istb2=null;
    
    if(sftpFileSend) {
    //logger.info("sftp put operation of "+props.getProperty("localFile") + " OK");
      logger.info("update pushed records in the dataBase "+headersArrLst1.size()+" item size: "+lineItemsArrLst1.size());
      for(int ax=0; ax<headersArrLst1.size(); ax++) {
        hstb2 = null;
        
        hstb2 = headersArrLst1.get(ax);
        try {
          pstmt3.setString(1,hstb2.getContractNumber());
          pstmt3.setObject(2,hstb2.getHeaderCtrlNumber());
          int rupd1=0;
          pstmt3.executeUpdate();  
          logger.info("Running Header update");
          recordsUpdated += rupd1;
          msg("hdr: ["+hstb2.getRowId()+"] ["+hstb2.getHeaderCtrlNumber()+"] rupd1["+rupd1+"] contract["+hstb2.getContractNumber()+"] doc#["+hstb2.getDocNumber()+"]");
        //dbconn.rollback();
          dbconn.commit();
        }catch(Exception e1) {
        //jReturnValue++;
          emsg="problem updating hdr ["+hstb2.getRowId()+"] ["+hstb2.getHeaderCtrlNumber()+"] contract["+hstb2.getContractNumber()+"]";
          devGroupMsgs += "\n" + emsg ; 
          msg("WARN: " +emsg);
          logger.warn(emsg);           
          emsg="";
        }

      }  
      for(int bx=0; bx<lineItemsArrLst1.size(); bx++) {
        istb2=null;
        
        istb2 = lineItemsArrLst1.get(bx);
        try {
          pstmt4.setString(1,istb2.getContractNumber());
          pstmt4.setObject(2,istb2.getHeaderCtrlNumber());
          int rupd2=0;
          pstmt4.executeUpdate();
          logger.info("Running Item update");
          recordsUpdated += rupd2;
          msg("itm: ["+istb2.getRowId()+"] ["+istb2.getHeaderCtrlNumber()+"] rupd2["+rupd2+"] contract["+istb2.getContractNumber()+"]");
          dbconn.commit();
        }catch(Exception e1) {
            //jReturnValue++;
            emsg="problem updating item ["+istb2.getRowId()+"] ["+istb2.getHeaderCtrlNumber()+"] contract["+istb2.getContractNumber()+"]";
            devGroupMsgs += "\n" + emsg ; 
            msg("WARN: " +emsg);
            logger.warn(emsg);           
            emsg="";

        }
      
      }   
    
    }
    else {
      if(lineItemsExtracted>0) {
     // send was attempted something went wrong 
        logger.error("sftp put operation of "+props.getProperty("remoteFile1") + " failed");
      }
      else if(lineitems>0) {
    	     // send was attempted something went wrong 
    	        logger.error("sftp put operation of "+props.getProperty("remoteFile2") + " failed");
    	      }else {
     // send was not attempted since no line items qualified
      //logger.info("no sftp put operation lineItemsExtracted["+lineItemsExtracted+"]");
        logger.info("no LineItems Extracted lineItemsExtracted["+lineItemsExtracted+"]");
      }
    }



    logger.info("contractNumbersExamined="+contractNumbersExamined);
    logger.info("headersExtracted=" + headersExtracted);
    logger.info("lineItemsExtracted=" + lineItemsExtracted);
    logger.info("recordsUpdated=" + recordsUpdated);
    
    devGroupMsgs += "\n" + "contractNumbersExamined="+contractNumbersExamined;
    devGroupMsgs += "\n" + "headersExtracted=" + headersExtracted;
    devGroupMsgs += "\n" + "lineItemsExtracted=" + lineItemsExtracted;
    devGroupMsgs += "\n" + "recordsUpdated=" + recordsUpdated;


    emsg += " - end";
    msg("mailing dev group");
    mailDevGroup(devGroupMsgs);
    msg("dev group mailed");

  } // end of runMe method


  private static void init() throws Exception {
   env = System.getProperty("tririgaEnv");

   log4jProps = getLogProperties();
// Set up configuration for logging.
// BasicConfigurator.configure();
   PropertyConfigurator.configure(log4jProps); // it works even without this line


// tririgaBatch.dev.cfg
  //msg(new java.util.Date() + ": loading properties");
    logger.info("start: loading properties. env=[" + env + "]");


    props = new Properties();

  //String configFile = cfgFileName + ".L0." + cfgFileExt;
  //String configFile = cfgFileName + "." + env + "." + cfgFileExt;
    String configFile = cfgFileName + "." + cfgFileExt;


  // from Hrt
    InputStream cfgFileStrm = null;
    cfgFileStrm = Thread.currentThread().getContextClassLoader().getResourceAsStream(configFile);

  //msg(configFile + " found");
    if(cfgFileStrm != null) {
      logger.debug(configFile + " found.");
    }else {
      logger.error(configFile + " NOT found");
    }
     
    String contents = streamToString(cfgFileStrm).toString();
    props.load(new DataInputStream(new ByteArrayInputStream(contents.getBytes())));

    initProps = true;


    InputStream fileStrm = null;
    fileStrm = Thread.currentThread().getContextClassLoader().getResourceAsStream(keyFileName);
    if(fileStrm != null) {
      logger.debug(keyFileName + " found");
    }else {
      logger.error(keyFileName + " NOT found");
    }

      
  //msg(new java.util.Date() + ": initializing encryption");
    logger.info("initializing encryption");

  //aes = new AesEncr(keyFileName);
  //aes.setKey(skey);
  //aes.instantiateCipher();

    emailHost       = props.getProperty("emailHost");
    excEmailTo      = props.getProperty("mailTo");
    excEmailFrom    = props.getProperty("mailFrom");
    emailSubjectExc = taskName + " " + env;

    String dbUrl = PropertyUtility.getProperty("ENCORE_JDBC_URL");
    String dbUser = PropertyUtility.getProperty("ENCORE_INTCLIENT_JDBC_USER");
  //String dbPasswdHash = PropertyUtility.getProperty("ENCORE_INTCLIENT_JDBC_PASSWORD");
    String dbPasswd = "";

  //dbPasswd = aes.deCrypt(dbPasswdHash); // decrypt
  //dbPasswd = PropertyUtility.getEncryptedProperty("ENCORE_INTCLIENT_JDBC_PASSWORD");
    dbPasswd="VCi2ClsSzf2BhoWKHochb66U7";
    msg("604 dbPasswd "+dbPasswd);
 // connect to the db

 // Load the Oracle JDBC driver
  //msg(new java.util.Date() + ": register jdbc driver");
    logger.info("register jdbc driver");
    DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());

  //msg(new java.util.Date() + ": get db connection");
    logger.info("get db connection");
    dbconn = DriverManager.getConnection (dbUrl,dbUser,dbPasswd);
    logger.info("Staging db connected");
    dbconn.setAutoCommit(false);
    initDbConn = true;
    dbPasswd =  null; // nuke it
    
    //For FCOE Db connect
    String dbUrl_fcoe = PropertyUtility.getProperty("FCOE_JDBC_URL");
    String dbUser_fcoe = PropertyUtility.getProperty("FCOE_INTCLIENT_JDBC_USER");
  //String dbPasswdHash = PropertyUtility.getProperty("ENCORE_INTCLIENT_JDBC_PASSWORD");
    String dbPasswd_fcoe = "";

  //dbPasswd = aes.deCrypt(dbPasswdHash); // decrypt
  //dbPasswd = PropertyUtility.getEncryptedProperty("ENCORE_INTCLIENT_JDBC_PASSWORD");
    dbPasswd_fcoe="F1xqbd5JB7Uo0MKor1PH9Jxe3";
    msg("604 dbPasswd "+dbPasswd_fcoe);
 // connect to the db

 // Load the Oracle JDBC driver
  //msg(new java.util.Date() + ": register jdbc driver");
    logger.info("register jdbc driver");
    DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());

  //msg(new java.util.Date() + ": get db connection");
    logger.info("get fcoe db connection");
    dbconn_fcoe = DriverManager.getConnection (dbUrl_fcoe,dbUser_fcoe,dbPasswd_fcoe);
    logger.info("FCOE db connected");
    dbconn_fcoe.setAutoCommit(false);
    initDbConn = true;
    dbPasswd_fcoe =  null; // nuke it

    sftpSiteHost = props.getProperty("SftpSiteHost");
    sftpSitePort = props.getProperty("SftpSitePort");
    sftpSiteUser = props.getProperty("SftpSiteUser");

    String sftpSitePwdParam = props.getProperty("SftpSitePwd");
  //sftpSitePwd = aes.deCrypt(sftpSitePwdParam); // decrypt
    
    
    
  } // end of init method

  private static Properties getLogProperties() throws Exception {

    Properties p = new Properties();
    Thread myThread = Thread.currentThread();
    ClassLoader loader = myThread.getContextClassLoader();
    InputStream istrm = loader.getResourceAsStream(log4jCfgFileName);

    String contents = streamToString(istrm).toString();
    p.load(new DataInputStream(new ByteArrayInputStream(contents.getBytes())));
    return p;
  } 


// from Hrt:
  /** ******************************************
   * <b>Purpose:</b> Converts the "lines" of data on a stream into a \n delimited string
   * so they can be passed around the rest of the program and manipulated.
   * Replaces the functionality from HrFileHandler.getFile(filename) which cannot read JAR's
   *
   * @throws Exception all general, non-trapped errors raised, including stream errors.
   * @param  <i>instream</i> input stream containing name/value pair parameter values.
   * @return StringBuffer = contents of specified file formatted as a \n delimited String.
   ******************************************** **/
 private static StringBuffer streamToString(InputStream instream) throws Exception {
     BufferedReader br = null;
     try {
        br = new BufferedReader(new InputStreamReader(instream));
        StringBuffer sbuf = new StringBuffer();
        String sLine = null;
        while ((sLine = br.readLine ()) != null) {
          sbuf.append(sLine + '\n');
        }
        return sbuf;
    // interesting ... no catch block
     }finally {
        if(br !=null) {br.close();}
     }
  }

  private static String getStack(Throwable t)  {
    String sout = "";
    StringWriter sw = new StringWriter();
    PrintWriter pw = new PrintWriter(sw);
    t.printStackTrace(pw);
    sout =  sw.toString();
    pw.close();
    try {
      sw.close();
    } catch(Exception exc) {}
      return sout;
  }


  private static void waitMills(int mills) {
    try {Thread.sleep(mills);}catch(Exception e){}
  }

  private static void msg(Object o) {
    System.out.println("" + o);
  }

  private static String nullCheck(String str){
    return str==null ? "" : str;
  }

  static BigDecimal nullCheck(BigDecimal bd) {
    return bd==null ? new BigDecimal(0) : bd;
  }

  static String getField(String in, String dl, int inum) {
    String sout = "";
    Vector v = GTokenizer1.tokenize1(in,dl);
    if(inum > 0 && inum <= v.size()) {
      sout = (String) v.elementAt(inum -1);
      sout = sout.trim();
    }
    return sout;
  }


  private static void mailDevGroup(String letter) {
    try {
      TrUtil.sendEmail(letter,emailSubjectExc,excEmailTo,null,emailHost,excEmailFrom);
    }catch(Exception e) {
      msg("sending email. caught:" + e + "\n" + getStack(e));
    }
  }

  private static boolean send() {
    boolean bval = false;

 // get params
    String localFile = props.getProperty("localFile");
    logger.info("Sending data to " + sftpSiteHost + ":" + sftpSitePort);

    String remotePath = "";
  //remotePath = getRemoteFilePath(props,sendTime);
    remotePath = props.getProperty("remoteFile");

 // ScpClient scp = null;
    SftpClient sftp = null;
    SSHHelper helper = null;
 // String keyFingerprint = "1030: 80 30 34 b3 82 98 7 12 f5 51 8e 9c f8 dd 62 b5"; // chsux09.corp.fedex.com

    int iPort = 22; // usual sftp default
    try {
      iPort = Integer.parseInt(sftpSitePort);
    }catch(Exception e) {
      logger.error("getting sftp port from cfg: " + e,e);
    }

   helper = new SSHHelper();

// Set connection attribute(s)
   helper.setVerbose(false); // Optional... for debugging
   helper.setKnownHostName(sftpSiteHost);
// helper.setKnownKeyFingerprint(keyFingerprint); // Optional... for validation

// Establish the connection
   logger.info("Connecting to " + sftpSiteHost + ":" + iPort);
   try {
  // helper.connectToServer();
     helper.connectToServer(iPort);
  // Compare hostname/key from server to known values. Optional step.
  // helper.validateServer();

   }catch(Exception e) {
     logger.error("connecting to " + sftpSiteHost + ":" + iPort + "  caught:" + e,e);
   }

   try {
  // scp = helper.getSCPClient(userName,userPasswd);  // Get a SCP client
     sftp = helper.getSFTPClient(sftpSiteUser,sftpSitePwd);  // Get a SCP client
   }catch(Exception e) {
     logger.error("getting sftp client for " + sftpSiteHost + ":" + iPort + "  caught:" + e,e);
   }

   logger.info("performing sftp.put  file -> " + remotePath);
   try {
  // scp.put(localFile,remotePath,false);  // do the file copy
     sftp.put(localFile,remotePath);  // do the file copy
     bval = true;
   }catch(Exception e) {
     logger.error("sftp put operation for " + sftpSiteHost + ":" + iPort + "  " + remotePath + "   caught:" + e,e);
   }

   // nuke
   sftpSiteUser = null; 
   sftpSitePwd = null;

   logger.info("sftp disconnecting");

   try {
     helper.disconnect(); // close the connection
   }catch(Exception e) {
     logger.error("disconnecting from " + sftpSiteHost + ". caught:" + e,e);
   }

   return bval;
  } // end of send() method

  static BigDecimal sumMyBigDecimals(BigDecimal[] bds) {  
    BigDecimal bd1 = new BigDecimal(0);
    for(int x=0; x<bds.length; x++) {
      bd1=bd1.add(bds[x]);
    }
    return bd1;
  }
  
  private static void cleanUp() {
  //msg(new java.util.Date() + ": closing connections and streams");
    logger.info("closing connections and streams");
 // close the database connection
    try {
      TrUtil.closeDbConn(dbconn);
    }catch(Exception e) {
    //msg("closing db connection: caught:" + e + "\n" + getStack(e));
      logger.error("closing db connection: caught:" + e + "\n" + getStack(e));
    }

 // nuke
    aes =  null;

  } // end cleanUp

  static void writeLineToFile(String in) throws Exception {
    File outf2 = new File(props.getProperty("remoteFile1"));
    FileWriter fr2 = new FileWriter(outf2,true);
    BufferedWriter buf = new BufferedWriter(fr2);   
    buf.write(in);
    buf.close();
  }
  
  static void writeCOToFile(String in) throws Exception {
	  logger.info("Writing file");
	    File outf2 = new File(props.getProperty("remoteFile2"));
	    logger.info("File path "+outf2.getAbsolutePath());
	    FileWriter fr2 = new FileWriter(outf2,true);
	    BufferedWriter buf = new BufferedWriter(fr2);   
	    buf.write(in);
	    buf.close();
	  }


}
