package com.fedex.encore.fcoe;

import java.math.BigDecimal;

public class HdrStgBean {
  private String rowId;
  private String contractNumber;
  private String docNumber;
  private String businessUnit;
  private String currency;
  private String vendorId;
  private String createdDate;
  private BigDecimal headerCtrlNumber;
  private BigDecimal tririga_RecordIdHdr;
  private boolean writtenOut;
  
  public String getRowId() {
    return rowId;
  }

  public void setRowId(String rowId) {
    this.rowId = rowId;
  }


  public BigDecimal getHeaderCtrlNumber() {
    return headerCtrlNumber;
  }

  public void setHeaderCtrlNumber(BigDecimal headerCtrlNumber) {
    this.headerCtrlNumber = headerCtrlNumber;
  }


  public String getContractNumber() {
    return contractNumber;
  }

  public void setContractNumber(String contractNumber) {
    this.contractNumber = contractNumber;
  }


  public String getDocNumber() {
    return docNumber;
  }

  public void setDocNumber(String docNumber) {
    this.docNumber = docNumber;
  }


  public String getBusinessUnit() {
    return businessUnit;
  }

  public void setBusinessUnit(String businessUnit) {
    this.businessUnit = businessUnit;
  }


  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }


  public String getVendorId() {
    return vendorId;
  }

  public void setVendorId(String vendorId) {
    this.vendorId = vendorId;
  }

  public boolean isWrittenOut() {
    return writtenOut;
  }

  public void setWrittenOut(boolean b) {
    this.writtenOut = b;
  }

  public String getCreatedDate() {
	  return createdDate;
  }

  public void setCreatedDate(String createdDate) {
	  this.createdDate = createdDate;
  }
  
  public BigDecimal gethdrTririgaRecID() {
	  return tririga_RecordIdHdr;
  }

  public void sethdrTririgaRecID(BigDecimal tririga_RecordIdHdr) {
	  this.tririga_RecordIdHdr = tririga_RecordIdHdr;
  }
}
