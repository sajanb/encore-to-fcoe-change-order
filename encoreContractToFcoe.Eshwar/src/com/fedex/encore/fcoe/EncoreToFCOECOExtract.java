package com.fedex.encore.fcoe;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class EncoreToFCOECOExtract {


	
	
public static String line_000(String businessUnit,String poStgId,String poRef,String currencyCd, String hdrCreatedDate, String hdrTririgaRecID) {
	
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	Calendar cal = Calendar.getInstance();
	String sysDate = dateFormat.format(cal.getTime());
	StringBuffer sb = new StringBuffer();
	
	sb.append("000");
	sb.append("|");
	
	//String eip_id = poStgId+poStgId;
  	sb.append(hdrCreatedDate+hdrTririgaRecID);
	//for(int k=0;k<(25-eip_id.length());k++)
	sb.append("|");
	
	sb.append(businessUnit);
	//for(int k=0;k<(5-businessUnit.length());k++)
	sb.append("|");
	
	sb.append(poStgId);
	//for(int k=0;k<(10-poStgId.length());k++)
	sb.append("|");
	
	sb.append("1");
	sb.append("|");
	
	sb.append(poRef);
	//for(int k=0;k<(30-poRef.length());k++)
	sb.append("|");
	
	sb.append(" ");
	sb.append("|");
	
	sb.append("1");
	sb.append("|");
	
	sb.append("HDQ");
	sb.append("|");
	
	sb.append("N");
	sb.append("|");
	
	sb.append("");
	sb.append("|");
	
	sb.append(currencyCd);
	//for(int k=0;k<(3-currencyCd.length());k++)
	sb.append("|");
	
	sb.append("CRRNT");
	sb.append("|");
	
	sb.append(" ");
	sb.append("|");
	
	sb.append(sysDate);
	sb.append("|");
	
	sb.append("PRN");
	sb.append("|");
	
	sb.append(sysDate);
	sb.append("|");
	
	sb.append("AT");
	sb.append("|");
	
	sb.append(sysDate);
	sb.append("|");
	
	sb.append("N");
	sb.append("|");
	
	sb.append(" ");
	sb.append("|");
	
	sb.append("ENCORE");
	sb.append("|");
	
	sb.append("N");
	sb.append("|");
	
	sb.append(" ");
	sb.append("|");
	
	sb.append(" ");
	sb.append("|");
	
	sb.append("BAC");
	sb.append("|");
	
	sb.append("EDI");
	sb.append("|");
	
	sb.append("N");
	sb.append("|");
	
	sb.append(" ");
	sb.append("|");
	
	sb.append(" ");
	sb.append("|");
	
	sb.append(" ");
	sb.append("\n");
	
	return sb.toString();
	}

public static String line_001(String busUnit,String poStageId,String lineNumber, String itemCreatedDate, String tririga_recordIdItem) {
		  StringBuffer sb = new StringBuffer();
		  
		  	sb.append("001");
			sb.append("|");
			
		  	//String eip_id = poStageId+poStageId;
		  	sb.append(itemCreatedDate+tririga_recordIdItem);
			//for(int k=0;k<(25-eip_id.length());k++)
			sb.append("|");
			
			sb.append(busUnit);
			//for(int k=0;k<(5-busUnit.length());k++)
			sb.append("|");
			
			sb.append(poStageId);
			//for(int k=0;k<(10-poStageId.length());k++)
			sb.append("|");
			
			sb.append(lineNumber);
			//for(int k=0;k<(5-lineNumber.length());k++)
			sb.append("|");
			
			sb.append(" ");
			sb.append("|");
			
			sb.append(" ");
			sb.append("|");
			
			sb.append(" ");
			sb.append("|");
			
			sb.append(" ");
			sb.append("|");
			
			sb.append(" ");
			sb.append("|");
			
			sb.append("EA");
			sb.append("|");
			
			sb.append(" ");
			sb.append("|");
			
			sb.append(" ");
			sb.append("|");
			
			sb.append(" ");
			sb.append("|");
			
			sb.append("0");
			sb.append("|");
			
			sb.append("0");
			sb.append("|");
			
			sb.append("0");
			sb.append("|");
			
			sb.append("0");
			sb.append("|");
			
			sb.append(" ");
			sb.append("|");
			
			sb.append("0");
			sb.append("|");
			
			sb.append("AT");
			sb.append("|");
			
			sb.append("N");
			sb.append("|");
			
			sb.append("0");
			sb.append("|");
			
			sb.append("0");
			sb.append("|");
			
			sb.append(" ");
			sb.append("\n");
			
		return sb.toString();
}

public static String line_002(String busUnit,String poStageId,String lineNumber,String pricePO, String dueDT, String qtyPO, String mechandiseAMT, String currencyCd, String itemCreatedDate, String tririga_recordIdItem) throws ParseException 
{
	String OLD_FORMAT = "dd-MMM-yyyy";
	String NEW_FORMAT = "yyyy/MM/dd";

	// August 12, 2010
	//String oldDateString = "12/08/2010";
	String newDateString;
	
	SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
	if(dueDT.equals(" "))
		newDateString=" ";
	else
	{
		Date d = sdf.parse(dueDT);
		sdf.applyPattern(NEW_FORMAT);
		newDateString = sdf.format(d);
	}
	  
	StringBuffer sb = new StringBuffer();
	  
	  
	  	sb.append("002");
		sb.append("|");
		
	  	//String eip_id = poStageId+poStageId;
	  	sb.append(itemCreatedDate+tririga_recordIdItem);
		//for(int k=0;k<(25-eip_id.length());k++)
		sb.append("|");
		
		sb.append(busUnit);
		//for(int k=0;k<(5-busUnit.length());k++)
		sb.append("|");
		
		sb.append(poStageId);
		//for(int k=0;k<(10-poStageId.length());k++)
		sb.append("|");
		
		sb.append(lineNumber);
		//for(int k=0;k<(5-lineNumber.length());k++)
		sb.append("|");
		
		sb.append("1");
		sb.append("|");
		
		sb.append(pricePO+".00");
		//for(int k=0;k<(14-pricePO.length());k++)
		sb.append("|");
		
		sb.append(newDateString);
		//for(int k=0;k<(10-dueDT.length());k++)
			//sb.append(" ");
		sb.append("|");
		
		sb.append(" ");
		sb.append("|");
		
		sb.append("0000028");
		sb.append("|");
		
		sb.append("1");
		sb.append("|");
		
		sb.append(mechandiseAMT);
		//for(int k=0;k<(28-mechandiseAMT.length());k++)
		sb.append("|");
		
		sb.append(currencyCd);
		//for(int k=0;k<(3-currencyCd.length());k++)
		sb.append("|");
		
		sb.append("DES");
		sb.append("|");
		
		sb.append("FEDEX");
		sb.append("|");
		
		sb.append("0");
		sb.append("|");
		
		sb.append("Y");
		sb.append("|");
		
		sb.append("N");
		sb.append("|");
		
		sb.append("N");
		sb.append("|");
		
		sb.append("0");
		sb.append("|");
		
		sb.append(" ");
		sb.append("|");
		
		sb.append(" ");
		sb.append("\n");
		
		return sb.toString();
}

public static String line_003(String poStageId, String bussUnit, String lineNumber, String glAccount, String costCenter, String projId, String pcBU, String activityId, String resourceType, String itemCreatedDate, String tririga_recordIdItem)
{
	StringBuffer sb = new StringBuffer();
	  
  	sb.append("003");
	sb.append("|");
	
	//String eip_id = poStageId+poStageId;
  	sb.append(itemCreatedDate+tririga_recordIdItem);
  	sb.append("|");
  	
  	
  	sb.append(bussUnit);
  	sb.append("|");
  	
  	sb.append(poStageId);
  	sb.append("|");
  	
  	sb.append(lineNumber);
  	sb.append("|");
  	
  	sb.append("1");
  	sb.append("|");
  	
  	
  	if(glAccount==null)
  		sb.append(" ");
  	else
  		sb.append(glAccount);
  	sb.append("|");
  	
  	if(costCenter==null)
  		sb.append(" ");
  	else
  		sb.append(costCenter);
  	sb.append("|");

  	
  	if(projId==null)
  		sb.append(" ");
  	else
  		sb.append(projId);
  	sb.append("|");
  	
 	if(pcBU==null)
  		sb.append(" ");
  	else
  		sb.append(pcBU);
  	sb.append("|");
  	
 	if(activityId==null)
  		sb.append(" ");
  	else
  		sb.append(activityId);
  	sb.append("|");
  	
 	if(resourceType==null)
  		sb.append(" ");
  	else
  		sb.append(resourceType);
  	sb.append("\n");

  	return sb.toString();
  	
}
}
