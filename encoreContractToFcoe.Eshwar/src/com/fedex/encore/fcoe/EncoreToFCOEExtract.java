package com.fedex.encore.fcoe;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

public class EncoreToFCOEExtract {
 static Logger log = Logger.getLogger(EncoreToFCOEExtract.class);
 static final String USD="USD";
 
 public static void main(String[] args) {
  System.out.println(createHdrStgRecord("10050", "PO55", "REF1", "USD", "VEND123", "PO134", "40.00", "10050", "USD"));
  System.out.println(createItmStgRecord("10050", "STGID", "1", "2013-JUL-10", "VENDID", "CATID", "EA", "1", "11.22", "223.00", "NET30", "222", "223.00", "USD", "BSE", "EA"));
 }

 
 public static String createHdrStgRecord(String businessUnit,String poStgId,String poRef,String currencyCd,String vendorId,
   String poId,String priceVendor,String busUnitGL,String currencyCdBase) {
  StringBuffer sb = new StringBuffer();
  //RECORD_TYPE
  sb.append("HDR_STG");
  sb.append("|");
  
  //BUSINESS_UNIT
  sb.append(nullCheck(businessUnit));
  sb.append("|");
  
  //PO_STG_ID
  sb.append(nullCheck(poStgId));
  sb.append("|");
  
  //PO_DT
  String formattedDate = new SimpleDateFormat("dd-MMM-yyyy").format(new Date());
  sb.append(formattedDate);  
  sb.append("|");   
  
  //PO_REF
  sb.append(nullCheck(poRef));
  sb.append("|");  
  
  //PYMNT_TERMS_CD
  sb.append(" ");
  sb.append("|");
  
  //BUYER_ID
  sb.append("ENCORE");
  sb.append("|");
  
  //ORIGIN
  sb.append("ENC");
  sb.append("|");
  
  //ADDRESS_SEQ_NUM
  sb.append("0");
  sb.append("|");
  
  
  //BILL_LOCATION
  sb.append("HDQ");
  sb.append("|");
  
  
  //TAX_EXEMPT
  sb.append("Y");
  sb.append("|");
  
  //TAX_EXEMPT_ID
  sb.append(" ");
  sb.append("|");
    
  //CURRENCY_CD
//sb.append(nullCheck(currencyCd));
  sb.append(currencyCd);
  sb.append("|");
  
  //VENDOR_ID
  sb.append(nullCheck(vendorId));
  sb.append("|");  
  
  //VNDR_LOC
  sb.append(" ");
   sb.append("|");  
  
  //PO_ID
  sb.append(nullCheck(poId));
  sb.append("|"); 
  
  //PO_BUILT_FLG
  sb.append("Y");
  sb.append("|");   
  
  //DISP_METHOD
  sb.append("PRN");
  sb.append("|");  
  
  //PRICE_VENDOR
  sb.append(nullCheck(priceVendor));
  sb.append("|");   
  
  //PRICE_LOC
  sb.append(" ");
  sb.append("|");   
  
  //ENTERED_DT
  sb.append(nullCheck(formattedDate));  
  sb.append("|");   
  
  //BUSINESS_UNIT_GL
  sb.append(nullCheck(busUnitGL));  
  sb.append("|");   
  
  //CURRENCY_CD_BASE
//sb.append(nullCheck(currencyCdBase));  
  sb.append(nullCheck(currencyCd));  
  sb.append("|");    
  
  //RATE_MULT
  sb.append("1");  
  sb.append("|");     
  
  //RATE_DIV
  sb.append("1");  
  sb.append("|");       
  
  //TEXT254_CC2
//sb.append(" ");  
  sb.append("ENCORE");  
    

  return sb.toString();
 }
 
 public static String createItmStgRecord(String busUnit,String poStageId,String lineNumber,String dueDt, String vendorId, String categoryId, 
   String unitOfMeasure, String qtyReq, String priceReq, String pricePo, String freightTerms, String poId, String merchAmt, 
   String currCd, String priceReqBse, String uom) {
  StringBuffer sb = new StringBuffer();
  
  //RECORD_TYPE
  sb.append("ITM_STG");
  sb.append("|");  
  
  //BUSINESS_UNIT
  sb.append(nullCheck(busUnit));
  sb.append("|");  
  
  //PO_STG_ID
  sb.append(nullCheck(poStageId));
  sb.append("|");    
  
  //LINE_NBR
  sb.append(nullCheck(lineNumber));
  sb.append("|");    
  
  //SCHED_NBR
  sb.append("1");
  sb.append("|");    
  
  //SHIPTO_ID
  sb.append("HD"); // was 0000028 
  sb.append("|");  
  
  //SUT_BASE_ID
  sb.append(" "); // was 0000028. requirements say 'Generated within PS'
  sb.append("|");    
  
  //SOURCE_DATE
  String formattedDate = new SimpleDateFormat("dd-MMM-yyyy").format(new Date());
  sb.append(formattedDate);  
  sb.append("|");     
  
  //DUE_DT
  sb.append(nullCheck(dueDt));
  sb.append("|");  
  
  //INV_ITEM_ID
  sb.append(" ");
  sb.append("|");    
  
  //DESCR254_MIXED
  sb.append(" ");
  sb.append("|");  
  
  //BUYER_ID
  sb.append("ENCORE");
  sb.append("|");
  
  //VENDOR_ID
  sb.append(nullCheck(vendorId));
  sb.append("|");  
  
  //VNDR_LOC
  sb.append(" ");
  sb.append("|");    
  
  //ITM_ID_VNDR
  sb.append(" ");
  sb.append("|");    
  
  //CATEGORY_ID
  sb.append(nullCheck(categoryId));
  sb.append("|");  
  
  //UNIT_OF_MEASURE
  sb.append(nullCheck(unitOfMeasure));
  sb.append("|");    
  
  //QTY_REQ
  sb.append(nullCheck(qtyReq));
  sb.append("|");   
  
  //PRICE_REQ
  sb.append(nullCheck(priceReq));
  sb.append("|");    
  
  //PRICE_PO
  sb.append(nullCheck(pricePo));
  sb.append("|");    
  
  //MFG_ID
  sb.append(" ");
  sb.append("|");    
  
  //MFG_ITM_ID
  sb.append(" ");
  sb.append("|");  
  
  //FREIGHT_TERMS
  sb.append(nullCheck(freightTerms));
  sb.append("|");  
  
  //CNTRCT_LINE_NBR
  sb.append("0");
  sb.append("|");  
  
  
  //MILESTONE_NBR
  sb.append("0");
  sb.append("|");
  
  //PCT_UNIT_PRC_TOL
  sb.append("0");
  sb.append("|");
  
  //PCT_UNIT_PRC_TOL_L
  sb.append("0");
  sb.append("|");
  
  //UNIT_PRC_TOL
  sb.append("0");
  sb.append("|");
  
  
  //UNIT_PRC_TOL_L
  sb.append("0");
  sb.append("|");
  
  //DISTRIB_MTHD_FLG
  sb.append("Q");
  sb.append("|");  
  
  //PO_ID
  sb.append(nullCheck(poId));
  sb.append("|");  
  
  //MERCHANDISE_AMT
  sb.append(nullCheck(merchAmt));
  sb.append("|");   
  
  //CURRENCY_CD
//sb.append(nullCheck(currCd));
  sb.append(currCd);
  sb.append("|");    
  
  //CURRENCY_CD_BASE
//sb.append(nullCheck(currCd));
  sb.append(currCd);
  sb.append("|");    
  
  //PRICE_REQ_BSE
  sb.append(nullCheck(priceReqBse));
  sb.append("|");    
  
  //QTY_REQ_STD
  sb.append(nullCheck(qtyReq));
  sb.append("|");     
  
  //UNIT_MEASURE_STD
  sb.append(nullCheck(uom));
  sb.append("|");    
  
  //ZERO_PRICE_IND
  sb.append("N");
  sb.append("|");  
  
  //RECV_REQ
  sb.append("N");
  
  return sb.toString();
  
 }
 
 public static String createDistStgRecord(String hdrBusUnit, String poStgId,String lineNbr,String qtyReq,String priceReq, String currencyCd, String merchAmt,String account,String deptId,String product, String itmBusUnit, String pcbu, String projectId, String activityId, String resourceType) {
  StringBuffer sb = new StringBuffer();
  
  //RECORD_TYPE
  sb.append("DIST_STG");
  sb.append("|");  
  
  //BUSINESS_UNIT
  sb.append(nullCheck(hdrBusUnit));
  sb.append("|");    
  
  //PO_STG_ID
  sb.append(nullCheck(poStgId));
  sb.append("|");  
  
  //LINE_NBR
  sb.append(nullCheck(lineNbr));
  sb.append("|");    
  
  //SCHED_NBR column 5
  sb.append("1");
  sb.append("|");    
    
  //DISTRIB_LINE_NUM
  sb.append("1");
  sb.append("|");  
  
  //QTY_PO
  sb.append(nullCheck(qtyReq));
  sb.append("|");  
  
  //QTY_REQ
  sb.append(nullCheck(priceReq));
  sb.append("|");    
  
  //CURRENCY_CD
//sb.append(nullCheck(currencyCd));
  sb.append(currencyCd);
  sb.append("|");    
  
  //CURRENCY_CD_BASE column 10
//sb.append(nullCheck(currencyCd));
  sb.append(currencyCd);
  sb.append("|");    
  
  //RATE_DIV
  sb.append("1");
  sb.append("|");     
  
  //RATE_MULT
  sb.append("1");
  sb.append("|");     
  
  //MERCHANDISE_AMT
  sb.append(nullCheck(merchAmt));
  sb.append("|");  
  
  //LOCATION
  sb.append("HDQ");
  sb.append("|");  
  
  //ACCOUNT column 15
  sb.append(nullCheck(account));
  sb.append("|");  
  
  //DEPTID
  sb.append(nullCheck(deptId));
  sb.append("|");    
  
  //PRODUCT
  sb.append(nullCheck(product));
  sb.append("|");    
  
  //BUSINESS_UNIT_GL
  sb.append(nullCheck(itmBusUnit));
  sb.append("|");    
  
  //QTY_PO_STD
  sb.append(nullCheck(priceReq));
  sb.append("|");

  //PC_BU column 20
  sb.append(nullCheck(pcbu));
  sb.append("|");

  //PROJECT_ID column 21
  sb.append(nullCheck(projectId));
  sb.append("|");

  //ACTIVITY_ID column 22
  sb.append(nullCheck(activityId));
  sb.append("|");

  //RESOURCE_TYPE column 23
  sb.append(nullCheck(resourceType));

  return sb.toString();
  
 }

 static String nullCheck(String s) {
   String out = "";
   if(s==null) {
     out="";
   }else if(s.toLowerCase().trim().equals("null")) {
     out="";
   }else {
     out=s;
   }
   return out;
 }
 
}
