package com.fedex.encore.fcoe;

import java.math.BigDecimal;

public class ItmStgBean {
  private String rowId;
  private String contractNumber;
  private String businessUnit;
  private String currency;
  private String vendorId;
  private BigDecimal headerCtrlNumber;
  
  public String getRowId() {
    return rowId;
  }

  public void setRowId(String rowId) {
    this.rowId = rowId;
  }


  public BigDecimal getHeaderCtrlNumber() {
    return headerCtrlNumber;
  }

  public void setHeaderCtrlNumber(BigDecimal headerCtrlNumber) {
    this.headerCtrlNumber = headerCtrlNumber;
  }


  public String getContractNumber() {
    return contractNumber;
  }

  public void setContractNumber(String contractNumber) {
    this.contractNumber = contractNumber;
  }


}
